//
// Created by luan on 27/10/2021.
//

#ifndef SIMGRIDTEMPLATEPROJECT_IO_SIM_H
#define SIMGRIDTEMPLATEPROJECT_IO_SIM_H

#include <simgrid/plugins/file_system.h>
#include <simgrid/s4u.hpp>
#include <filesystem>

#define INMEGA (1024 * 1024)
#define INGIGA (1024 * 1024 * 1024)

#endif //SIMGRIDTEMPLATEPROJECT_IO_SIM_H
