//
// Created by luan on 27/10/2021.
//

#ifndef SIMGRIDTEMPLATEPROJECT_IO_SCHEDULER_H
#define SIMGRIDTEMPLATEPROJECT_IO_SCHEDULER_H

#include "simgIO.h"
#include <xbt/random.hpp>
#include <vector>


class IOScheduler {

private:
    // vector of all running activities
    std::vector<std::pair<simgrid::s4u::IoPtr, double>> running;
    // vector of all activities that are waiting
    std::vector<std::pair<simgrid::s4u::IoPtr, double>> waiting;

    // Time trackers
    std::map<simgrid::s4u::IoPtr, double> start_time = {};
    std::map<simgrid::s4u::IoPtr, double> end_time = {};

    // A map used to guarantee that applications with the same priority are not running together
    std::map<double, bool> priority_locker;



    // indicates that someone else is modifying the vectors
    simgrid::s4u::SemaphorePtr semaphore = simgrid::s4u::Semaphore::create(1);

    void suspend_all();

    void resume_all();


public:
    void print_all();

    bool has_priority(double priority);

    void add_activity(const simgrid::s4u::IoPtr &new_activity, double priority);

    void remove_activity(const simgrid::s4u::IoPtr &old_activity, double priority);

    static void
    remove_it(std::vector<std::pair<simgrid::s4u::IoPtr, double>> &activities, const simgrid::s4u::IoPtr &to_remove);

    double get_io_start(const simgrid::s4u::IoPtr &activity);

    double get_io_end(const simgrid::s4u::IoPtr &activity);

    double get_runtime(const simgrid::s4u::IoPtr &activity);
};


#endif //SIMGRIDTEMPLATEPROJECT_IO_SCHEDULER_H
