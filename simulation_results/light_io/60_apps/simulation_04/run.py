#!/usr/bin/env python3
import math


from PYsimgio.app_generator import APPGenerator
from PYsimgio.evaluation import Evaluation

"""
Set mapping
"""


def set10_mapping(generator: APPGenerator):
    for app in generator.apps:
        group_id = round(math.log10(app.w_iter))
        app.set_set_id(group_id)


def clairvoyant_mapping(generator: APPGenerator):
    for app in generator.apps:
        app.set_set_id(math.log10(app.mu))


def exclusive_mapping(generator: APPGenerator):
    for app in generator.apps:
        app.set_set_id(1)


def fairShare_mapping(generator: APPGenerator):
    for app in generator.apps:
        app.set_set_id(1 + app.app_id * 10 ** (-6))


"""
Set priority
"""


# everyone receives the same priority (1)
def f_exclusive_fcfs(generator: APPGenerator):
    for app in generator.apps:
        app.set_priority(app.set_id)


# each app receive a different priority, so everyone executes together
def f_fair_share(generator: APPGenerator):
    for app in generator.apps:
        app.set_priority(app.set_id)


def f_set_10(generator: APPGenerator):
    for app in generator.apps:
        app.set_priority(1 / (10 ** app.set_id))


def tasks_per_mu(n_apps):
    return 1 / 4, [15, 15, 15, 15]


#group_functions = {"set_mapping": set_mapping, "clairvoyant": clairvoyant_mapping}

priority_functions = {'Set-10': (set10_mapping, f_set_10),
                      'Clairvoyant': (clairvoyant_mapping, f_set_10),
                      'Exclusive-FCFS': (exclusive_mapping, f_exclusive_fcfs),
                      'FairShare': (fairShare_mapping, f_fair_share)
                      }

walking_mu = [5, 10, 20, 50, 100, 150, 250,
              500, 1000, 1500, 2000, 3000, 4000, 5000]
fixed_mu = [10, 100, 1000]

# Cleaning everything!
Evaluation.clean_all()

for mu in walking_mu:
    mu_list = fixed_mu.copy()
    mu_list.append(mu)
    mu_list = sorted(mu_list)

    test = Evaluation(evaluation_csv='evaluation.csv',
                      n_executions=20,
                      n_apps=60,
                      mu_list=mu_list,
                      sd_percent=0.1,
                      io_limit=0.2,
                      const=20000,
                      tasks_per_mu=tasks_per_mu,
                      timeframe_start=6000, timeframe_end=14000,
                      with_desyn=True,
                      with_noise=True,
                      platform_file=f"{Evaluation.SIMGIO_HOME}/simulation_results/host_with_disk.xml")

    #test.set_group_functions(group_functions)
    test.set_priority_functions(priority_functions)
    test.run_test(folder_id=f'clairvoyant_{mu}')
