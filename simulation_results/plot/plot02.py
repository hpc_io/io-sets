import matplotlib.pyplot as plt

import PYsimgio.simgio_utils as su
import pandas as pd
import argparse
from scipy import stats

def generate_graph(TEST_HOME, n_tasks):
    n_executions = 30
    #n_tasks = 60
    #TEST_HOME = f"../heavy_io/{n_tasks}_apps/simulation_02/"

    priorities = ['Exclusive-FCFS', 'FairShare', 
    #'Sharing+Priority', 
    #'Set-FairShare', 
    'Set-10']



    df10, ev10 = su.load_all(priorities=priorities, n_tasks=n_tasks, n_executions=n_executions,
                        csv_folder=f'{TEST_HOME}csv_0.100')
    df20, ev20 = su.load_all(priorities=priorities, n_tasks=n_tasks, n_executions=n_executions,
                        csv_folder=f'{TEST_HOME}csv_0.200')
    df30 , ev30 = su.load_all(priorities=priorities, n_tasks=n_tasks, n_executions=n_executions,
                        csv_folder=f'{TEST_HOME}csv_0.300')
    df40 , ev40 = su.load_all(priorities=priorities, n_tasks=n_tasks, n_executions=n_executions,
                        csv_folder=f'{TEST_HOME}csv_0.400')
    df50 , ev50 = su.load_all(priorities=priorities, n_tasks=n_tasks, n_executions=n_executions,
                        csv_folder=f'{TEST_HOME}csv_0.500')
    df60 , ev60 = su.load_all(priorities=priorities, n_tasks=n_tasks, n_executions=n_executions,
                        csv_folder=f'{TEST_HOME}csv_0.600')
    df70 , ev70 = su.load_all(priorities=priorities, n_tasks=n_tasks, n_executions=n_executions,
                        csv_folder=f'{TEST_HOME}csv_0.700')
    df80 , ev80 = su.load_all(priorities=priorities, n_tasks=n_tasks, n_executions=n_executions,
                        csv_folder=f'{TEST_HOME}csv_0.800')
    df90 , ev90 = su.load_all(priorities=priorities, n_tasks=n_tasks, n_executions=n_executions,
                        csv_folder=f'{TEST_HOME}csv_0.900')
    df1_00, ev1_00 = su.load_all(priorities=priorities, n_tasks=n_tasks, n_executions=n_executions,
                            csv_folder=f'{TEST_HOME}csv_1.000')

    noise_values = [0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 1.0]
    ev_list = [ev10, ev20, ev30, ev40, ev50, ev60, ev70, ev80, ev90, ev1_00]

    df = pd.concat([ev10, ev20, ev30, ev40, ev50, ev60, ev70, ev80, ev90, ev1_00])

    df_io =  pd.concat([df10, df20, df30, df40, df50, df60, df70, df80, df90, df1_00])
    df_io = su.compute_metrics(df_io, t_begin=6000, t_end=14000)


    df_fairshare = df.loc[df.priority == "FairShare"]
    df_exclusive = df.loc[df.priority == "Exclusive-FCFS"]
    df_set10 = df.loc[df.priority == "Set-10"]

    df_all = {
        "FairShare": df_fairshare,
        "Set-10": df_set10,
        "Exclusive-FCFS": df_exclusive
    }


    label_fontSize = 32
    tick_labelSize = 28
    legend_fontSize = 30


    def utilization_graph():

        fig, ax = plt.subplots(1, figsize=(17, 8))


        colors = ["blue", "green", "red"]

        color_index = 0
        for key, value in df_all.items():

            ax.scatter(value.noise, value.utilization, color=colors[color_index], alpha=0.6, label=key)
            y_values = []
            for v in noise_values:
                initial = v
                final = v + 0.10
                y_values.append(
                    value.loc[(value.noise >= initial) & (value.noise <= final)].utilization.mean()
                )
            ax.plot(noise_values, y_values, color=colors[color_index], linewidth=3)

            color_index += 1

        # ax.set_facecolor("lightgray")


        plt.ylabel(r'Utilization', fontsize=label_fontSize, fontweight='bold')
        plt.xlabel(r'Noise percentage $b$', fontweight='bold', fontsize=label_fontSize)

        plt.grid()

        # ax.set_yscale('log')
        #lgnd = plt.legend(prop={'size': legend_fontSize})
        lgnd = plt.legend(prop={'size': legend_fontSize})
        lgnd.legendHandles[0]._sizes = [300]
        lgnd.legendHandles[1]._sizes = [300]
        lgnd.legendHandles[2]._sizes = [300]

        ax.set_xticks(noise_values)

        # plt.title(title, fontweight='bold', fontsize=18)

        ax.tick_params(axis='both', which='major', labelsize=tick_labelSize)
        ax.tick_params(axis='both', which='minor', labelsize=tick_labelSize)

        fig.savefig(f"utilization_noise_{n_tasks}.png", format='png', dpi=1200)
    
    def stretch_graph():

        fig, ax = plt.subplots(1, figsize=(17, 8))


        colors = ["blue", "green", "red"]

        color_index = 0
        for key, value in df_all.items():

            ax.scatter(value.noise, value.max_stretch, color=colors[color_index], alpha=0.6, label=key)
            y_values = []
            for v in noise_values:
                initial = v
                final = v + 0.10
                y_values.append(
                    value.loc[(value.noise >= initial) & (value.noise <= final)].max_stretch.mean()
                )
            ax.plot(noise_values, y_values, color=colors[color_index], linewidth=3)

            color_index += 1

        # ax.set_facecolor("lightgray")


        plt.ylabel(r'Max Stretch', fontsize=label_fontSize, fontweight='bold')
        plt.xlabel(r'Noise percentage $b$', fontweight='bold', fontsize=label_fontSize)

        plt.grid()

        # ax.set_yscale('log')
        #lgnd = plt.legend(prop={'size': legend_fontSize})
        lgnd = plt.legend(prop={'size': legend_fontSize})
        lgnd.legendHandles[0]._sizes = [300]
        lgnd.legendHandles[1]._sizes = [300]
        lgnd.legendHandles[2]._sizes = [300]

        ax.set_xticks(noise_values)

        # plt.title(title, fontweight='bold', fontsize=18)

        ax.tick_params(axis='both', which='major', labelsize=tick_labelSize)
        ax.tick_params(axis='both', which='minor', labelsize=tick_labelSize)

        fig.savefig(f"max_stretch_noise_{n_tasks}.png", format='png', dpi=1200)

    def io_slowdown_norm(g, df, colors=None, show=False, save_to=None,  figsize=(17, 8), yticks=None, ylim=None):
        from matplotlib.ticker import StrMethodFormatter
        from matplotlib.ticker import FormatStrFormatter
        import matplotlib.ticker as mtick
        from scipy import stats

        """
        Receives a dictionary with pandas dataframes and generates the scatter graph
        x-axis  shows the number of applications in the Nh set and the y-axis presents the max stretch
        :param g: a dictionary whose keys are the scheduling heuristic names and values are dataframes
        :param title: graph titles (optional)
        :param save_to:  full path where the file (pdf) will be saved (optional)
        :param figsize: the size of the graph (optional)
        """


        fig, ax = plt.subplots(1, figsize=figsize)
        plt.subplots_adjust(top=0.96)

        


        # for each element in g, plot a scatter
        color_index = 0

        x_values =  df.groupby('noise').groups.keys()
        
        for key, value in g.items():         

            value['io_slowdown_norm'] = (1 - (value.io_slowdown.values / df.io_slowdown.values)) * 100

            ax.scatter(x_values, value.groupby(["noise"]).io_slowdown_norm.mean(), color=colors[color_index], alpha=0.6, label=key)
            
            y_values = []
            for v in x_values:
                initial = v
                final = v + 0.5
                y_values.append(
                    value.loc[(value.noise >= initial) & (value.noise <= final)].io_slowdown_norm.mean()
                )
            ax.plot(x_values, y_values, color=colors[color_index], linewidth=3)

            color_index += 1        

        plt.ylabel(r'Relative I/O Slowdown', fontsize=28, fontweight='bold')
        plt.xlabel(r'Noise percentage $b$', fontweight='bold', fontsize=28)
        #plt.xticks(rotation=45)
        

        lgnd = plt.legend(prop={'size': legend_fontSize})
        for i in range(len(lgnd.legendHandles)):
            lgnd.legendHandles[i]._sizes = [300]        


        ax.yaxis.set_major_formatter(mtick.PercentFormatter())        
        
        ax.tick_params(axis='both', which='major', labelsize=28)
        ax.tick_params(axis='both', which='minor', labelsize=28)        

        ax.set_yticks(yticks)
        plt.ylim(ylim)
        #ax.yaxis.set_major_formatter()

        plt.grid()

        if save_to:
            fig.savefig(save_to, format=save_to.split(".")[-1], dpi=1200)
        if show:
            plt.show()



    utilization_graph()
    # stretch_graph()

    # io_slowdown_norm({
    #     "Exclusive-FCFS": df_io.loc[df_io.priority=="Exclusive-FCFS"],
    #     "FairShare": df_io.loc[df_io.priority=="FairShare"],
    #     },    
    #     df=df_io.loc[df_io.priority=="Set-10"],
    # figsize=(17,8),
    # colors=['red', 'blue'],
    # yticks=[i for i in range(-700, 50, 100)],
    # save_to=f"io_slowdown_noise_{n_tasks}.pdf")


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('path', help="full_path to the tests", type=str)
    parser.add_argument('n_tasks', help="number of tasks in the test", type=int)

    args = parser.parse_args()

    generate_graph(TEST_HOME=args.path, n_tasks=args.n_tasks)



if __name__ == "__main__":
    main()
