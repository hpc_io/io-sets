import matplotlib.pyplot as plt
from numpy import var

import PYsimgio.simgio_utils as su
import pandas as pd

import argparse


def generate_graph(TEST_HOME, n_tasks):

    n_executions = 100
    #n_tasks = 60
    #TEST_HOME = f"../heavy_io/{n_tasks}_apps/simulation_03/"
    #groups = ['set_mapping']
    priorities = ['Exclusive-FCFS', 'FairShare', 
    #'Sharing+Priority', 
    #'Set-FairShare', 
    'Set-10']

    df05, ev05 = su.load_all(priorities=priorities, n_tasks=n_tasks,  n_executions=n_executions,
                        csv_folder=f'{TEST_HOME}csv_0.050')
    df10, ev10 = su.load_all(priorities=priorities, n_tasks=n_tasks,  n_executions=n_executions,
                        csv_folder=f'{TEST_HOME}csv_0.100')
    df20, ev20 = su.load_all(priorities=priorities, n_tasks=n_tasks,  n_executions=n_executions,
                        csv_folder=f'{TEST_HOME}csv_0.200')
    df30, ev30 = su.load_all(priorities=priorities, n_tasks=n_tasks,  n_executions=n_executions,
                        csv_folder=f'{TEST_HOME}csv_0.300')
    df40, ev40 = su.load_all(priorities=priorities, n_tasks=n_tasks,  n_executions=n_executions,
                        csv_folder=f'{TEST_HOME}csv_0.400')
    df50, ev50 = su.load_all(priorities=priorities, n_tasks=n_tasks,  n_executions=n_executions,
                        csv_folder=f'{TEST_HOME}csv_0.500')
    df60, ev60 = su.load_all(priorities=priorities, n_tasks=n_tasks,  n_executions=n_executions,
                        csv_folder=f'{TEST_HOME}csv_0.600')
    df70, ev70 = su.load_all(priorities=priorities, n_tasks=n_tasks,  n_executions=n_executions,
                        csv_folder=f'{TEST_HOME}csv_0.700')
    df80, ev80 = su.load_all(priorities=priorities, n_tasks=n_tasks,  n_executions=n_executions,
                        csv_folder=f'{TEST_HOME}csv_0.800')
    df90, ev90 = su.load_all(priorities=priorities, n_tasks=n_tasks,  n_executions=n_executions,
                        csv_folder=f'{TEST_HOME}csv_0.900')
    df1_00, ev1_00 = su.load_all(priorities=priorities, n_tasks=n_tasks,  n_executions=n_executions,
                            csv_folder=f'{TEST_HOME}csv_1.000')

    ev05['sigma'] = 0.05
    ev10['sigma'] = 0.10
    ev20['sigma'] = 0.20
    ev30['sigma'] = 0.30
    ev40['sigma'] = 0.40
    ev50['sigma'] = 0.50
    ev60['sigma'] = 0.60
    ev70['sigma'] = 0.70
    ev80['sigma'] = 0.80
    ev90['sigma'] = 0.90
    ev1_00['sigma'] = 1.000


    df05['sigma'] = 0.05
    df10['sigma'] = 0.10
    df20['sigma'] = 0.20
    df30['sigma'] = 0.30
    df40['sigma'] = 0.40
    df50['sigma'] = 0.50
    df60['sigma'] = 0.60
    df70['sigma'] = 0.70
    df80['sigma'] = 0.80
    df90['sigma'] = 0.90
    df1_00['sigma'] = 1.000


    # ax.set_facecolor("lightgray")


    label_fontSize = 28
    tick_labelSize = 28
    legend_fontSize = 28


    #ax.set_facecolor("lightgray")
    variability_values = [0.05, 0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 1.0]
    ev_list = [ev05, ev10, ev20, ev30, ev40, ev50, ev60, ev70, ev80, ev90, ev1_00]

    df = pd.concat(ev_list)


    df_io = pd.concat([df05, df10, df20, df30, df40, df50, df60, df70, df80, df90, df1_00])

    df_io = su.compute_metrics(df=df_io, t_begin=6000, t_end=14000)


    df_fairshare = df.loc[df.priority == "FairShare"]
    df_exclusive = df.loc[df.priority == "Exclusive-FCFS"]
    df_set10 = df.loc[df.priority == "Set-10"]

    df_all = {
        "FairShare": df_fairshare,
        "Set-10": df_set10,
        "Exclusive-FCFS": df_exclusive
    }



    def utilization():

        fig, ax = plt.subplots(1, figsize=(17, 13))

        colors = ["blue", "green", "red"]

        color_index = 0
        for key, value in df_all.items():

            ax.scatter(value.sigma, value.utilization, color=colors[color_index], alpha=0.6, label=key)
            y_values = []
            for v in variability_values:
                initial = v
                final = v + 0.10
                y_values.append(
                    value.loc[(value.sigma >= initial) & (value.sigma <= final)].utilization.mean()
                )
            ax.plot(variability_values, y_values, color=colors[color_index], linewidth=3)

            color_index += 1

        plt.ylabel(r'Utilization', fontsize=label_fontSize, fontweight='bold')
        plt.xlabel(r'$\sigma/\mu$', fontweight='bold', fontsize=label_fontSize)

        # ax.set_yscale('log')
        lgnd = plt.legend(prop={'size': legend_fontSize})
        lgnd.legendHandles[0]._sizes = [300]
        lgnd.legendHandles[1]._sizes = [300]
        lgnd.legendHandles[2]._sizes = [300]
        plt.grid()

        plt.xticks(variability_values, rotation=45)

        ax.tick_params(axis='both', which='major', labelsize=tick_labelSize)
        ax.tick_params(axis='both', which='minor', labelsize=tick_labelSize)

        fig.savefig(f"sigma_test_utilization_{n_tasks}.png", format='png', dpi=1200)
    
    def stretch():

        fig, ax = plt.subplots(1, figsize=(17, 13))

        colors = ["blue", "green", "red"]

        color_index = 0
        for key, value in df_all.items():

            ax.scatter(value.sigma, value.max_stretch, color=colors[color_index], alpha=0.6, label=key)
            y_values = []
            for v in variability_values:
                initial = v
                final = v + 0.10
                y_values.append(
                    value.loc[(value.sigma >= initial) & (value.sigma <= final)].max_stretch.mean()
                )
            ax.plot(variability_values, y_values, color=colors[color_index], linewidth=3)

            color_index += 1

        plt.ylabel(r'Max Stretch', fontsize=label_fontSize, fontweight='bold')
        plt.xlabel(r'$\sigma/\mu$', fontweight='bold', fontsize=label_fontSize)

        # ax.set_yscale('log')
        lgnd = plt.legend(prop={'size': legend_fontSize})
        lgnd.legendHandles[0]._sizes = [300]
        lgnd.legendHandles[1]._sizes = [300]
        lgnd.legendHandles[2]._sizes = [300]
        plt.grid()

        plt.xticks(variability_values, rotation=45)

        ax.tick_params(axis='both', which='major', labelsize=tick_labelSize)
        ax.tick_params(axis='both', which='minor', labelsize=tick_labelSize)

        fig.savefig(f"sigma_test_stretch_{n_tasks}.pdf", format='pdf', dpi=1200)

    def io_slowdown_norm(g, df, colors=None, show=False, save_to=None,  figsize=(17, 8), yticks=None, ylim=None):
        from matplotlib.ticker import StrMethodFormatter
        from matplotlib.ticker import FormatStrFormatter
        import matplotlib.ticker as mtick
        from scipy import stats

        """
        Receives a dictionary with pandas dataframes and generates the scatter graph
        x-axis  shows the number of applications in the Nh set and the y-axis presents the max stretch
        :param g: a dictionary whose keys are the scheduling heuristic names and values are dataframes
        :param title: graph titles (optional)
        :param save_to:  full path where the file (pdf) will be saved (optional)
        :param figsize: the size of the graph (optional)
        """


        fig, ax = plt.subplots(1, figsize=figsize)
        plt.subplots_adjust(top=0.96)

       


        # for each element in g, plot a scatter
        color_index = 0

        x_values =  df.groupby('sigma').groups.keys()
        
        for key, value in g.items():         

            value['io_slowdown_norm'] = (1 - (value.io_slowdown.values / df.io_slowdown.values)) * 100

            ax.scatter(x_values, value.groupby(["sigma"]).io_slowdown_norm.mean(), color=colors[color_index], alpha=0.6, label=key)
            
            y_values = []
            for v in x_values:
                initial = v
                final = v + 0.5
                y_values.append(
                    value.loc[(value.sigma >= initial) & (value.sigma <= final)].io_slowdown_norm.mean()
                )
            ax.plot(x_values, y_values, color=colors[color_index], linewidth=3)

            color_index += 1        

        plt.ylabel(r'Relative I/O Slowdown', fontsize=28, fontweight='bold')
        plt.xlabel(r'$\sigma/\mu$', fontweight='bold', fontsize=28)
        #plt.xticks(rotation=45)
     

        lgnd = plt.legend(prop={'size': legend_fontSize})
        for i in range(len(lgnd.legendHandles)):
            lgnd.legendHandles[i]._sizes = [300]        


        ax.yaxis.set_major_formatter(mtick.PercentFormatter())        
        
        ax.tick_params(axis='both', which='major', labelsize=28)
        ax.tick_params(axis='both', which='minor', labelsize=28)        

        ax.set_yticks(yticks)
        plt.ylim(ylim)
        #ax.yaxis.set_major_formatter()

        plt.grid()

        if save_to:
            fig.savefig(save_to, format=save_to.split(".")[-1], dpi=1200)
        if show:
            plt.show()


    utilization()
    #stretch()

    # io_slowdown_norm({
    #         "Exclusive-FCFS": df_io.loc[df_io.priority=="Exclusive-FCFS"],
    #         "FairShare": df_io.loc[df_io.priority=="FairShare"],
    #         },    
    #         df=df_io.loc[df_io.priority=="Set-10"],
    #     figsize=(17,8),
    #     colors=['red', 'blue'],
    #     yticks=[i for i in range(-700, 50, 100)],
    #     save_to=f"io_slowdown_sigma_{n_tasks}.pdf")
    




def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('path', help="full_path to the tests", type=str)
    parser.add_argument('n_tasks', help="number of tasks in the test", type=int)

    args = parser.parse_args()

    generate_graph(TEST_HOME=args.path, n_tasks=args.n_tasks)



if __name__ == "__main__":
    main()
