import sys
import os

import PYsimgio.simgio_utils as su
import matplotlib.pyplot as plt
import pandas as pd
from scipy import stats

from matplotlib.ticker import StrMethodFormatter, NullFormatter
from collections import OrderedDict
from pathlib import Path
from typing import Dict, List
from scipy import stats

import math
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.ticker import StrMethodFormatter
import matplotlib.ticker as mtick
from matplotlib.patches import Patch
import matplotlib.lines as mlines



def load_data(nh_list, heuristic):
    df_all = pd.DataFrame()
    for nh in nh_list:
        df = pd.read_csv(f"../comparing_real/{nh}/info_{heuristic}.csv")
        df['nh'] = nh
        df['execution_id'] = nh

        df = df.rename(columns={'stretch': 'app_stretch'})

        df_all = pd.concat([df_all, df], ignore_index=True)
        
    
    return df_all


def plot_all(practical, simulated, colors, title=None, show=False, save_to=None, figsize=(17, 8),  x_plot=None, granularity=2):
    # plot the max stretch and the utilization in two graphs in the same figure
    fig, ax = plt.subplots(1, 2, figsize=figsize)
    fig.subplots_adjust(wspace=0.3, left=0.08, right=0.96, bottom=0.12, top=0.88)

    # ploting the max stretch
    # for each element in g, plot a scatter
    color_index = 0
    for key, value in simulated.items():        
        label = '_nolegend_'
        gb = value.groupby('nh')
        ax[0].plot(gb.groups.keys(), gb.app_stretch.max(), '--', color=colors[color_index],linewidth=3, label=label, alpha=0.6)
        color_index += 1

    # Printing Practical Set-10    
    value = practical['Set-10']
    results = []
    for nh in x_plot:
        results.append((value.groupby("nh").get_group(nh).max_stretch.values))
    ax[0].boxplot(results, positions=x_plot)  

    # Printing the actual execution
    # Printing Practical Set-10    
    value = practical['Actual Execution']
    ax[0].scatter(value.nh, value.max_stretch, color="blue",  alpha=0.6)
    
    y_values = []
    for v in x_plot:
        initial = v
        final = v + granularity
        y_values.append(stats.gmean(value.loc[(value.nh >= initial) & (value.nh <= final)].max_stretch))
    
    ax[0].plot(x_plot, y_values, "x-", color="blue", linewidth=3, label=key)        
    # xlabel on ax[0]
    ax[0].set_xlabel(r'$\mathcal{n}_H$', fontweight='bold', fontsize=12)    
    # ylabel on ax[0]
    ax[0].set_ylabel(r'Max Stretch', fontsize=12, fontweight='bold')

    # increase tick size for all axes in both graphs
    for a in ax:
        # set the labelsize for y axis
   
        a.tick_params(axis='y', which='major', labelsize=12)
        a.tick_params(axis='y', which='minor', labelsize=12) 

        a.tick_params(axis='x', which='major', labelsize=11)
        a.tick_params(axis='x', which='minor', labelsize=11) 
        # turn x-axis 90 degrees
        #a.set_xticklabels(a.get_xticks(), rotation=45)


    # ploting the utilization
    y_values = {}
    color_index = 0
    for key, value in simulated.items():       
        ax[1].scatter(value.nh, value.utilization, color=colors[color_index], alpha=0.6)
        color_index += 1
        y_values[key] = []
        for v in x_plot:
            initial = v
            final = v + granularity
            y_values[key].append(value.loc[(value.nh >= initial) & (value.nh <= final)].utilization.mean())

    color_index = 0
    for key, value in simulated.items():
        label = '_nolegend_'
        simulation_plot = ax[1].plot(x_plot, y_values[key], '--', color=colors[color_index], label=label, linewidth=3)
        color_index += 1
    
    # Printing Practical Set-10    
    value = practical['Set-10']
    results = []
    for nh in x_plot:
        results.append((value.groupby("nh").get_group(nh).utilization.values))
    ax[1].boxplot(results, positions=x_plot)  
    
    # Printing the actual execution
    # # Printing Practical Set-10    
    value = practical['Actual Execution']
    ax[1].scatter(value.nh, value.utilization, color="blue",  alpha=0.6)    
    y_values = []
    if x_plot:
       for v in x_plot:
               initial = v
               final = v + granularity
               y_values.append(
                   stats.gmean(value.loc[(value.nh >= initial) & (value.nh <= final)].utilization)
               )
    
    ax[1].plot(x_plot, y_values, "x-", color="blue", linewidth=3, label=key)
    
    # xlabel on ax[1]
    ax[1].set_xlabel(r'$\mathcal{n}_H$', fontweight='bold', fontsize=12)
    # ylabel on ax[1]
    ax[1].set_ylabel(r'Utilization', fontsize=12, fontweight='bold')
    
    # Generate legend on the top of the figure
    handles = [
         Patch(facecolor="orange", edgecolor="k", label="Set-10 on Bora",  fill=False), 
         mlines.Line2D([], [], color='green', linestyle='--', linewidth=1.5, label=r'Set-10 with SimGIO'),
         mlines.Line2D([], [], color='blue', linestyle='-', linewidth=1.5, label=r'System Scheduler')
         ]

      # Set custom tick positions on the x-axis
    custom_xticks = range(0, 17)
    ax[0].set_xticks(custom_xticks)
    ax[1].set_xticks(custom_xticks)

    custom_labels = ['0', '1', '', '', '4', '', '','','8','','','','12','','','','16']

    # Hide tick labels on the x-axis
    ax[0].set_xticklabels(custom_labels)
    ax[1].set_xticklabels(custom_labels)


    lgnd = plt.legend(handles=handles, bbox_to_anchor=(-0.14, 1), loc='lower center', ncol=4, prop={'size': 12, 'weight': 'bold'}, handlelength=1.4, handleheight=1.4)

    # Adjust the position and appearance of the legend handles
    for handle in lgnd.legendHandles:
        handle._sizes = [100]


    if save_to:
        fig.savefig(save_to, format=save_to.split(".")[-1], dpi=1200)







# load simulation results
nh_list = [0, 1,  4,  8,  12, 15, 16]

colors = ['blue', 'red', 'green']

t_begin = 1300
t_end = 6300

exclusive = load_data(nh_list=nh_list, heuristic="exclusive")
fairshare = load_data(nh_list=nh_list, heuristic="fairshare")
set10 = load_data(nh_list=nh_list, heuristic="set10")

exclusive = su.compute_metrics(exclusive, t_begin=t_begin, t_end=t_end)
fairshare = su.compute_metrics(fairshare, t_begin=t_begin, t_end=t_end)
set10 = su.compute_metrics(set10, t_begin=t_begin, t_end=t_end)

# load practical results
df = pd.read_csv("../../practical_results/results/8hf_8lf_set10_0_phases.csv", sep=';')
columns = ['scheduler', 'hf_apps', 'lf_apps',  'max_stretch', 'utilization']
#df = df[columns]

df = df.rename({'hf_apps': 'nh'}, axis=1)

df_exclusive = df.loc[df.scheduler == 'exclusive']
df_fairshare = df.loc[df.scheduler == 'fairshare']
df_set10 = df.loc[df.scheduler == 'set10']

x_plot = [0, 1, 4, 8, 12, 16]
#colors = ['blue', 'red', 'green']
colors = [ 'green']




simulated= {    
    "Set-10": set10
}


practical = {
    'Actual Execution': df_fairshare,    
     'Set-10': df_set10}




#plot_stretch_max(practical=practical,simulated=simulated, title="Max Stretch",
#x_plot = nh_list,
#colors=colors,
#save_to="stretch_practicalxSimulated.pdf",
#granularity=1,
#figsize=(17,8))

#plot_scatter_utilization(practical=practical, simulated=simulated, title="Utilization",
#colors=colors,  figsize=(17,8), save_to="utilization_practivalxSimulated.pdf", 
# x_plot=x_plot)

plot_all(practical=practical,simulated=simulated,
x_plot = nh_list,
colors=colors,
save_to="practicalxSimulated.png",
granularity=1,
figsize=(8,4))