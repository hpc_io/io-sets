import matplotlib.pyplot as plt
import pandas as pd
import statistics
from matplotlib.ticker import StrMethodFormatter
from typing import Dict

pd.options.mode.chained_assignment = None

import PYsimgio.simgio_utils as su
from scipy import stats

import argparse


def generate_graph(test_folder, n_tasks):

    n_executions = 200
    #n_tasks = 60
    tbegin = 6000
    tend = 14000

    #test_folder = f"../heavy_io/{n_tasks}_apps/simulation_01/"

    utilization_upperbound = 1 - (0.8/n_tasks)

    fig_type="png"


    x_plot=[i for i in range(0, round(2/3 * n_tasks ) + 1)]

    priorities = ['Exclusive-FCFS',
                'FairShare',
                'Sharing+Priority',
                'Set-FairShare',
                'Set-10']

    # Load all the data
    df_all, ev = su.load_all(priorities=priorities,
                            n_tasks=n_tasks,
                            n_executions=n_executions,
                            csv_folder=test_folder+"csv")

    df_all = su.compute_metrics(df_all, t_begin=tbegin, t_end=tend)



    # plotting graph
    colors = ['blue', 'red', 'green']


    """
    Graphs considering FairShare, Exclusive and Set-10
    """

    # Utilization 
    su.plot_scatter_utilization(
        {"FairShare": ev.loc[ev.priority=="FairShare"],
        "Exclusive-FCFS": ev.loc[ev.priority=='Exclusive-FCFS'],
        "Set-10": ev.loc[ev.priority=='Set-10']},
        figsize=(20,8),
        #figsize=(30,30),
        colors=colors,
        x_plot=x_plot,
        granularity=2,
        #ylim=(0.8, 1),
        a=0.01,
        b=1,
        upper_bound=utilization_upperbound,
        save_to=f"utilization_{n_tasks}.{fig_type}")



    # Max Stretch 
    su.plot_stretch_max(
        {"FairShare": ev.loc[ev.priority=="FairShare"],
        "Exclusive-FCFS": ev.loc[ev.priority=='Exclusive-FCFS'],
        "Set-10": ev.loc[ev.priority=='Set-10']},
        figsize=(17,8),
        colors=colors,
        x_plot=x_plot,
        ylim=(1, 2.2),
        granularity=2,
        save_to=f"max_stretch_{n_tasks}.{fig_type}", legend=False)


    su.plot_io_slowdown_norm(
        {
            "Exclusive-FCFS": df_all.loc[df_all.priority=='Exclusive-FCFS'],
            "FairShare": df_all.loc[df_all.priority=='FairShare'] },
        df = df_all.loc[df_all.priority == "Set-10"],
        figsize=(19,8),
        colors=['red', 'blue'],
        yticks=[i for i in range(-250, 100, 50)], ylim=(-500,0),
        save_to=f"io_slowdown_relative_{n_tasks}.{fig_type}", legend=False)




    """
    Graphs considering Set-10, Sharing-Priority and Set-FairShare
    """

    colors_others = ['orange', 'violet',  'green']

    # Max Stretch
    su.plot_stretch_max(
        {"Set-FairShare": ev.loc[ev.priority=="Set-FairShare"],
        "Sharing+Priority": ev.loc[ev.priority=='Sharing+Priority'],
        "Set-10": ev.loc[ev.priority=='Set-10']},
        figsize=(17,10),
        colors=colors_others,
        x_plot=x_plot,
        granularity=2,
        save_to=f"max_stretch_others_{n_tasks}.{fig_type}")
    
    # su.plot_scatter_utilization(
    #     {"Set-FairShare": ev.loc[ev.priority=="Set-FairShare"],
    #     "Sharing+Priority": ev.loc[ev.priority=='Sharing+Priority'],
    #     "Set-10": ev.loc[ev.priority=='Set-10']},
    #     figsize=(17,10),
    #     colors=colors_others,
    #     x_plot=x_plot,
    #     granularity=2,
    #     save_to=f"utilization_others_{n_tasks}.{fig_type}")
    
    # su.plot_io_slowdown_norm(
    #     {
    #         "Set-FairShare": df_all.loc[df_all.priority=="Set-FairShare"],
    #          "Sharing+Priority": df_all.loc[df_all.priority=='Sharing+Priority']},
    #     df = df_all.loc[df_all.priority == "Set-10"],
    #     figsize=(19,8),
    #     colors=['orange', 'violet'],
    #     yticks=[i for i in range(-250, 150, 50)], ylim=(-200,100),
    #     save_to=f"io_slowdown_others_relative_{n_tasks}.{fig_type}", legend=False)





def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('path', help="full_path to the tests", type=str)
    parser.add_argument('n_tasks', help="number of tasks in the test", type=int)

    args = parser.parse_args()

    generate_graph(test_folder=args.path, n_tasks=args.n_tasks)



if __name__ == "__main__":
    main()
