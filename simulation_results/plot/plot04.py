    
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import pandas as pd

import argparse
import PYsimgio.simgio_utils as su



def generate_graph(TEST_HOME, n_tasks):
    n_executions = 20
    #n_tasks = 60
    #TEST_HOME =f"../heavy_io/{n_tasks}_apps/simulation_04"

    priorities = ['Set-10', 'Exclusive-FCFS', 'FairShare', 'Clairvoyant']
    walking_mu = [5, 10, 20,  50, 100, 150, 250,  500, 1000, 1500, 2000, 3000, 4000, 5000]


    # Loading the data
    df_all = pd.DataFrame()
    df_full = pd.DataFrame()

    for mu in walking_mu:
        df, ev = su.load_all(priorities=priorities, n_tasks=n_tasks, n_executions=n_executions, csv_folder=f'{TEST_HOME}/csv_clairvoyant_{mu}')  
        ev['mu'] = mu
        df['mu'] = mu
        df_all = pd.concat([df_all, ev])
        df_full = pd.concat([df_full, df])

    # drop duplicated tests
    #df_all = df_all.drop(df_all[(df_all.group=='clairvoyant') & (df_all.priority !='Set-10')].index)
    #df_full = df_full.drop(df_full[(df_full.group=='clairvoyant') & (df_full.priority !='Set-10')].index)

    df_full = su.compute_metrics(df_full, t_begin=6000, t_end=14000)


    df_full_clair = df_full.loc[df_full['priority'] == 'Clairvoyant'].reset_index()
    df_full_exclusive = df_full.loc[df_full['priority'] == 'Exclusive-FCFS'].reset_index()
    df_full_share = df_full.loc[df_full['priority'] == 'FairShare'].reset_index()
    df_full_set10 = df_full.loc[df_full['priority'] == 'Set-10'].reset_index()

    df_full_exclusive['norm_slowdown'] = 1 - ((df_full_exclusive.io_slowdown/ df_full_clair.io_slowdown))
    df_full_share['norm_slowdown'] = 1 - ((df_full_share.io_slowdown/ df_full_clair.io_slowdown)) 
    df_full_set10['norm_slowdown'] = 1 - ((df_full_set10.io_slowdown/ df_full_clair.io_slowdown)) 
    df_full_clair['norm_slowdown'] = 1 - ((df_full_clair.io_slowdown/ df_full_clair.io_slowdown))   


    df_clair = df_all.loc[df_all['priority'] == 'Clairvoyant'].reset_index()
    df_exclusive = df_all.loc[df_all['priority'] == 'Exclusive-FCFS'].reset_index()
    df_share = df_all.loc[df_all['priority'] == 'FairShare'].reset_index()
    df_set10 = df_all.loc[df_all['priority'] == 'Set-10'].reset_index()


    df_exclusive['norm_utilization'] = (1 - (df_exclusive.utilization/ df_clair.utilization) )
    df_share['norm_utilization'] = 1 - ((df_share.utilization/ df_clair.utilization)) 
    df_set10['norm_utilization'] = 1 - ((df_set10.utilization/ df_clair.utilization)) 
    df_clair['norm_utilization'] = 1 - ((df_clair.utilization/ df_clair.utilization))   

    df_exclusive['norm_stretch'] = (1 - (df_exclusive.max_stretch/ df_clair.max_stretch) )
    df_share['norm_stretch'] = 1 - ((df_share.max_stretch/ df_clair.max_stretch)) 
    df_set10['norm_stretch'] = 1 - ((df_set10.max_stretch/ df_clair.max_stretch)) 
    df_clair['norm_stretch'] = 1 - ((df_clair.max_stretch/ df_clair.max_stretch))   





    g = {
        "Exclusive-FCFS": df_exclusive,
        "FairShare": df_share,
        "Set-10": df_set10
    }

    label_fontSize = 34
    tick_labelSize = 28
    legend_fontSize = 32


    def utilization():
    
        fig, ax = plt.subplots(1, figsize=(22, 14))

        colors = ["red", "blue", "green"]

        color_index = 0
        for key, value in g.items():

            ax.scatter(value.mu, value.norm_utilization.apply(lambda x : x * 100), color=colors[color_index], alpha=0.6, label=key)
            y_values = []
            for v in walking_mu:
                initial = v
                final = v + 10
                y_values.append(
                    value.loc[(value.mu >= initial) & (value.mu <= final)].norm_utilization.apply(lambda x : x * 100).mean()
                )
            ax.plot(walking_mu, y_values, color=colors[color_index], linewidth=3)

            color_index += 1


        plt.ylabel(r'relative utilization difference', fontsize=label_fontSize, fontweight='bold')
        plt.xlabel(r'$\mu \in [5, 5000]$', fontweight='bold', fontsize=label_fontSize)
        plt.xticks(rotation=45)
        plt.grid()

        #ax.set_yscale('log')
        ax.set_xscale('log')
        lgnd = plt.legend(prop={'size': legend_fontSize})
        lgnd.legendHandles[0]._sizes = [300]
        lgnd.legendHandles[1]._sizes = [300]
        lgnd.legendHandles[2]._sizes = [300]


        #ax.set_ylim([-75, 10])
        ax.yaxis.set_major_formatter(mtick.PercentFormatter())

        ax.set_xticks(walking_mu)
        ax.set_xticklabels([str(mu) for mu in walking_mu])
        ax.tick_params(axis='both', which='major', labelsize=tick_labelSize)
        ax.tick_params(axis='both', which='minor', labelsize=tick_labelSize)

        fig.savefig(f"clairvoyant_utilization_norm_{n_tasks}.png", format='png', dpi=1200)


    def stretch():
    
        fig, ax = plt.subplots(1, figsize=(22, 14))

        colors = ["red", "blue", "green"]

        color_index = 0
        for key, value in g.items():

            ax.scatter(value.mu, value.norm_stretch.apply(lambda x : x * 100), color=colors[color_index], alpha=0.6, label=key)
            y_values = []
            for v in walking_mu:
                initial = v
                final = v + 10
                y_values.append(
                    value.loc[(value.mu >= initial) & (value.mu <= final)].norm_stretch.apply(lambda x : x * 100).mean()
                )
            ax.plot(walking_mu, y_values, color=colors[color_index], linewidth=3)

            color_index += 1


        plt.ylabel(r'relative stretch difference', fontsize=label_fontSize, fontweight='bold')
        plt.xlabel(r'$\mu \in [5, 5000]$', fontweight='bold', fontsize=label_fontSize)
        plt.xticks(rotation=45)
        plt.grid()

        #ax.set_yscale('log')
        ax.set_xscale('log')
        lgnd = plt.legend(prop={'size': legend_fontSize})
        lgnd.legendHandles[0]._sizes = [300]
        lgnd.legendHandles[1]._sizes = [300]
        lgnd.legendHandles[2]._sizes = [300]


        #ax.set_ylim([-75, 10])
        ax.yaxis.set_major_formatter(mtick.PercentFormatter())

        ax.set_xticks(walking_mu)
        ax.set_xticklabels([str(mu) for mu in walking_mu])
        ax.tick_params(axis='both', which='major', labelsize=tick_labelSize)
        ax.tick_params(axis='both', which='minor', labelsize=tick_labelSize)

        fig.savefig(f"clairvoyant_stretch_norm_{n_tasks}.pdf", format='pdf', dpi=1200)

    def io_slowdown_norm(g, df, colors=None,  figsize=(17, 13)):
        from matplotlib.ticker import StrMethodFormatter
        from matplotlib.ticker import FormatStrFormatter
        import matplotlib.ticker as mtick
        from scipy import stats

        """
        Receives a dictionary with pandas dataframes and generates the scatter graph
        x-axis  shows the number of applications in the Nh set and the y-axis presents the max stretch
        :param g: a dictionary whose keys are the scheduling heuristic names and values are dataframes
        :param title: graph titles (optional)
        :param save_to:  full path where the file (pdf) will be saved (optional)
        :param figsize: the size of the graph (optional)
        """
        
        fig, ax = plt.subplots(1, figsize=figsize)
        plt.subplots_adjust(top=0.96)

       


        # for each element in g, plot a scatter
        color_index = 0

        x_values =  df.groupby('mu').groups.keys()
        
        for key, value in g.items():         

            value['io_slowdown_norm'] = (1 - (value.io_slowdown.values / df.io_slowdown.values)) * 100

            ax.scatter(x_values, value.groupby(["mu"]).io_slowdown_norm.mean(), color=colors[color_index], alpha=0.6, label=key)
            
            y_values = []
            for v in x_values:
                initial = v
                final = v + 5
                y_values.append(
                    value.loc[(value.mu >= initial) & (value.mu <= final)].io_slowdown_norm.mean()
                )
            ax.plot(x_values, y_values, color=colors[color_index], linewidth=3)

            color_index += 1        

        plt.ylabel(r'Relative I/O Slowdown', fontsize=28, fontweight='bold')
        plt.xlabel(r'$\mu \in [5, 5000]$', fontweight='bold', fontsize=label_fontSize)
        plt.xticks(rotation=45)
        plt.grid()

        #ax.set_yscale('log')
        ax.set_xscale('log')
        lgnd = plt.legend(prop={'size': legend_fontSize})
        for i in range(len(lgnd.legendHandles)):
            lgnd.legendHandles[i]._sizes = [300]
        #lgnd.legendHandles[1]._sizes = [300]
        #lgnd.legendHandles[2]._sizes = [300]


        #ax.set_ylim([-75, 10])
        ax.yaxis.set_major_formatter(mtick.PercentFormatter())

        ax.set_xticks(walking_mu)
        ax.set_xticklabels([str(mu) for mu in walking_mu])
        ax.tick_params(axis='both', which='major', labelsize=tick_labelSize)
        ax.tick_params(axis='both', which='minor', labelsize=tick_labelSize)

        fig.savefig(f"clairvoyant_slowdown_{n_tasks}.pdf", format='pdf', dpi=1200)


    utilization()
    # stretch()
    # io_slowdown_norm({
    #         "Exclusive-FCFS": df_full_exclusive,
    #         "FairShare": df_full_share,
    #         "Set-10": df_full_set10,
    #         #"Clairvoyant": df_full_clair 
    #         },    
    #         df=df_full_clair,
    #     figsize=(19,14),
    #     colors=['red', 'blue', "green"])
        




def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('path', help="full_path to the tests", type=str)
    parser.add_argument('n_tasks', help="number of tasks in the test", type=int)

    args = parser.parse_args()

    generate_graph(TEST_HOME=args.path, n_tasks=args.n_tasks)



if __name__ == "__main__":
    main()
