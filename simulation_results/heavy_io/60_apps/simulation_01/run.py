#!/usr/bin/env python3
import math
import random

from PYsimgio.app_generator import APPGenerator
from PYsimgio.evaluation import Evaluation

"""
Set mapping
"""


def set10_mapping(generator: APPGenerator):
    for app in generator.apps:
        group_id = round(math.log10(app.w_iter))
        app.set_set_id(group_id)


def exclusive_mapping(generator: APPGenerator):
    for app in generator.apps:
        app.set_set_id(1)


def fairShare_mapping(generator: APPGenerator):
    for app in generator.apps:
        app.set_set_id(1 + app.app_id * 10 ** (-6))


"""
Set priority
"""


# everyone receives the same priority (1)
def f_exclusive_fcfs(generator: APPGenerator):
    for app in generator.apps:
        app.set_priority(app.set_id)


# each app receive a different priority, so everyone executes together
def f_fair_share(generator: APPGenerator):
    for app in generator.apps:
        app.set_priority(app.set_id)


def f_all_sharing_with_priority(generator: APPGenerator):
   for app in generator.apps:
       app.set_priority(1 / (app.w_iter + app.app_id * 10 ** (-6)))


# each app in a set receives the same priority.
# Each set has a different priority
def f_set_fair_share(generator: APPGenerator):
   for app in generator.apps:
       app.set_priority(1 + app.set_id * 10 ** (-6))


def f_set_10(generator: APPGenerator):
    for app in generator.apps:
        app.set_priority(1 / (10 ** app.set_id))


def tasks_per_mu(n_apps):
    x = random.uniform(0, 2 / 3)

    # Using the randomly defined value 'x', we define the number of tasks generated per mu value
    small = int(round(x * n_apps))  # small groups
    medium = int(round((1 / 3) * n_apps))  # medium group
    large = n_apps - medium - small  # large group

    return x, [small, medium, large]


priority_functions = {'Exclusive-FCFS': (exclusive_mapping, f_exclusive_fcfs),
                      'FairShare': (fairShare_mapping, f_fair_share),
                      'Sharing+Priority': (fairShare_mapping, f_all_sharing_with_priority),
                      'Set-FairShare': (set10_mapping, f_set_fair_share),
                      'Set-10': (set10_mapping, f_set_10)}

test = Evaluation(evaluation_csv='evaluation.csv',
                  n_executions=200,
                  n_apps=60,
                  mu_list=[10, 100, 1000],
                  sd_percent=0.1,
                  io_limit=0.8,
                  const=20000,
                  tasks_per_mu=tasks_per_mu,
                  timeframe_start=6000, timeframe_end=14000,
                  with_desyn=True,
                  with_noise=True,
                  platform_file=f"{Evaluation.SIMGIO_HOME}/simulation_results/host_with_disk.xml")

Evaluation.clean_all()

test.set_priority_functions(priority_functions)
test.run_test()
