import pandas as pd

import PYsimgio.simgio_utils as su

n_executions = 200
groups = ['set_mapping']
priorities = ['Exclusive-FCFS',
              'FairShare',
              'Sharing+Priority',
              'Set-FairShare',
              'Set-10']

# getting results of simulation_01
df_all, ev = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                         csv_folder='heavy_load/simulation_01/csv/')

s1_totaltime = sum(df_all.groupby('execution_id').end_app.max())

# getting results of simulation_02

n_executions = 30
groups = ['set_mapping']
priorities = ['Exclusive-FCFS', 'FairShare', 'Sharing+Priority', 'Set-FairShare', 'Set-10']

ev0, _ = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                     csv_folder='heavy_load/simulation_02/csv_0.0/')
ev10, _ = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                      csv_folder='heavy_load/simulation_02/csv_0.100')
ev20, _ = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                      csv_folder='heavy_load/simulation_02/csv_0.200')
ev30, _ = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                      csv_folder='heavy_load/simulation_02/csv_0.300')
ev40, _ = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                      csv_folder='heavy_load/simulation_02/csv_0.400')
ev50, _ = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                      csv_folder='heavy_load/simulation_02/csv_0.500')
ev60, _ = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                      csv_folder='heavy_load/simulation_02/csv_0.600')
ev80, _ = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                      csv_folder='heavy_load/simulation_02/csv_0.800')
ev90, _ = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                      csv_folder='heavy_load/simulation_02/csv_0.900')
ev1_00, _ = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                        csv_folder='heavy_load/simulation_02/csv_1.000')

g = {'0.00': ev0,
     '0.10': ev10,
     '0.20': ev20,
     '0.30': ev30,
     '0.40': ev40,
     '0.50': ev50,
     '0.60': ev60,
     '0.80': ev80,
     '0.90': ev90,
     '1.00': ev1_00
     }

s2_totaltime = 0
for key, df_all in g.items():
    s2_totaltime += sum(df_all.groupby('execution_id').end_app.max())

# simulation 3

n_executions = 100
groups = ['set_mapping']
priorities = ['Exclusive-FCFS', 'FairShare', 'Sharing+Priority', 'Set-FairShare', 'Set-10']

ev05, _ = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                      csv_folder='heavy_load/simulation_03/csv_0.050')
ev10, _ = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                      csv_folder='heavy_load/simulation_03/csv_0.100')
ev20, _ = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                      csv_folder='heavy_load/simulation_03/csv_0.200')
ev30, _ = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                      csv_folder='heavy_load/simulation_03/csv_0.300')
ev40, _ = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                      csv_folder='heavy_load/simulation_03/csv_0.400')
ev50, _ = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                      csv_folder='heavy_load/simulation_03/csv_0.500')
ev60, _ = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                      csv_folder='heavy_load/simulation_03/csv_0.600')
ev80, _ = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                      csv_folder='heavy_load/simulation_03/csv_0.800')
ev90, _ = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                      csv_folder='heavy_load/simulation_03/csv_0.900')
ev1_00, _ = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                        csv_folder='heavy_load/simulation_03/csv_1.000')

g = {'0.05': ev05,
     '0.10': ev10,
     '0.20': ev20,
     '0.30': ev30,
     '0.40': ev40,
     '0.50': ev50,
     '0.60': ev60,
     '0.80': ev80,
     '0.90': ev90,
     '1.00': ev1_00
     }

s3_totaltime = 0
for key, df_all in g.items():
    s3_totaltime += sum(df_all.groupby('execution_id').end_app.max())

# Simulation 4
n_executions = 20
groups = ['set_mapping', 'clairvoyant']
priorities = ['Set-10', 'Exclusive-FCFS', 'FairShare']
walking_mu = [5, 10, 20, 50, 100, 150, 250, 500, 1000, 1500, 2000, 3000, 4000, 5000]
# Loading the data
df_all = pd.DataFrame()
df_full = pd.DataFrame()

s4_totaltime = 0
for mu in walking_mu:
    df, ev = su.load_all(priorities=priorities, groups=groups, n_executions=n_executions,
                         csv_folder=f'heavy_load/simulation_04/csv_clariant_{mu}')
    s4_totaltime += sum(df.groupby('execution_id').end_app.max())

print("Heavy Load")
print(f"Simulation 01: {s1_totaltime}s")
print(f"Simulation 02: {s2_totaltime}s")
print(f"Simulation 03: {s3_totaltime}s")
print(f"Simulation 04: {s3_totaltime}s")

print(f"Total Time: {s1_totaltime + s2_totaltime + s3_totaltime + s4_totaltime}s")
print(f"Total Time: {(s1_totaltime + s2_totaltime + s3_totaltime + s4_totaltime)/60}m")
print(f"Total Time: {(s1_totaltime + s2_totaltime + s3_totaltime + s4_totaltime)/60**2}h")
print(f"Total Time: {((s1_totaltime + s2_totaltime + s3_totaltime + s4_totaltime)/60**2)/24}D")
