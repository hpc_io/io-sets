#!/usr/bin/env python3
import csv
import math
import statistics
from datetime import date

from numpy import random

"""
This class JUST generate apps

I decide to make it a separate class to be used as the basis of all the tests we intend to create.
In this way, we have the guarantee that applications will be created following the same recipe.

 
The best way to describe this class is: we can create a set of tasks, group it in diffent ways and apply different 
priority functions to classify them. It is pretty cool class :)

"""


class APPGenerator:
    class App:
        def __init__(self, app_id, t_comp, t_io, n_iter, w_iter, alpha_io, mu, desyn_time):
            """
            A class that represents an application
            :param app_id: the ID of the app
            :param t_comp: the time of computation in the app iteration
            :param t_io:  the time of IO in the app iteration
            :param n_iter: number of iterations of the app
            :param w_iter: size of the iteration of the app (w_iter = t_comp + t_io)
            :param alpha_io:  percentage of the iteration time that will be spent performing
            I/O (t_io = alpha_io * w_iter)
            :param mu: the mu (mean) value used to generated the task            
            :param desyn_time: delay added before the first iteration to create a desynchronization on applications start time
            """

            self.app_id = app_id
            self.t_comp = t_comp
            self.t_io = t_io
            self.n_iter = n_iter
            self.w_iter = w_iter
            self.alpha_io = alpha_io
            self.mu = mu
            self.desyn_time = desyn_time

            self.priority = None
            self.set_id = None
            # self.mu_list = mu_list

        def set_priority(self, priority: float):
            self.priority = priority

        def set_set_id(self, set_id: int):
            self.set_id = set_id

        def get_priority(self):
            return self.priority

        def get_set_id(self):
            return self.set_id

        # ATTENTION: those conversions are hardcoded and make sense in the links
        # capacities and CPU power described in the host_with_disk.xml file; if other environments or other proprieties
        # are used in the simulation, those functions do not work anymore!
        @property
        def compute_size(self):
            # converts the t_comp to the compute_size used in simgio

            return f"{max(math.floor(self.t_comp), 1)}e9"

        @property
        def io_size(self):
            # converts the t_io to the io_size used in simgio
            return f"{self.t_io * 10 ** 9}"

        def __str__(self):
            return f"\t# App_id: {self.app_id:.2f} Witer:{self.w_iter:.2f} Iter:{self.n_iter} " \
                   f"Alpha_io: {self.alpha_io:.4f}  T_io:{self.t_io:.2f} T_comp:{self.t_comp:.2f} " \
                   f"desyn_time: {self.desyn_time}"

    def __init__(self, mu_values, tasks_per_mu, io_limit, sd_percent, const, timeframe_start, timeframe_end, with_desyn=False, fixed_alphaio=False, alpha_io=0.1):
        # a list with the values of mu
        # The values in the mu_list are the mean or expectation
        # of the normal distribution used to generate the witer values
        self.mu_values = mu_values

        # a list with the values to defining how many tasks need to be generated per mu value
        self.tasks_per_mu = tasks_per_mu

        assert len(self.tasks_per_mu) == len(
            self.mu_values), "Error: len(tasks_per_mu) != len(mu values)"

        # value between 0 and 1 that determine the total amount of IO considering all apps
        self.io_limit = io_limit

        # value used to defined the total time of the apps
        # all apps have the same total time, but different iteration sizes
        self.const = const

        # Percentage used to define the stand deviation of the normal distribution used to generate the w_iter
        # The stand deviation is given by sd_percent * mu_values[set_id], i.e. the mean of the normal distribution
        self.sd_percentage = sd_percent

        # flag used to insert a waiting time before the execution of the app's first iteration
        self.with_desyn = with_desyn

        # flag used to indicate that alpha_io will not be generated randomly, instead, it will give as input
        self.fixed_alphaio = fixed_alphaio
        self.alpha_io = alpha_io

        # used to ensure that all applications will start before the timeframe
        self.timeframe_start = timeframe_start

        # used to ensure that all applications will finish after the timeframe
        self.timeframe_end = timeframe_end

        # list with all generated apps
        self.apps = []
        # Generate all apps
        self.__generate_apps()

    def __str__(self):
        return f"\n\t# This file was generated using the APPGenerator class\n" \
               f"\t# Date: {date.today()}\n" \
               f"\t# The following parameters were used:\n" \
               f"\t#  mu_values: {self.mu_values} " \
               f"\t#  tasks_per_mu: {self.tasks_per_mu} sd_percentage: {self.sd_percentage} " \
               f"\t#  const: {self.const} io_limit: {self.io_limit} \n"

    def __generate_apps(self):

        # group_id for the ts list
        app_id = 0

        # For each app, we first generate a random value between 0 and 1
        ts = random.uniform(0, 1, sum(self.tasks_per_mu))

        # for each group, we start generating the tasks
        index = 0
        io_load = 0.0
        alpha_io = self.alpha_io   # that value will be used if fixed_alphaio is true
        
        while index < len(self.mu_values):
            # First, we get the mu value of the group
            mu = self.mu_values[index]

            # Then, we generate the parameters for all the tasks belonging to that group
            # tasks_per_mu[index] -> number of tasks in the group
            for i in range(self.tasks_per_mu[index]):
                # total time of one iteration of the app (CPU + IO)
                w_iter = random.normal(mu, self.sd_percentage * mu)

                while w_iter < 1:
                    print(f"Dropping invalid Witer: {w_iter}..")
                    w_iter = random.normal(mu, self.sd_percentage * mu)

                # desyn_time
                desyn_time = 0
                if self.with_desyn:
                    desyn_time = random.uniform(0, 1) * w_iter

                # number of iterations
                n_iter = math.ceil(self.const / w_iter)

                # check if the application will start before the time frame
                assert desyn_time < self.timeframe_start, f"Error: app_{app_id} will start  ({desyn_time}) after the T_begin ({self.timeframe_start})"

                # check if the application  will finishe after the time frame
                assert (desyn_time + w_iter *
                        n_iter) > self.timeframe_end, f"Error: app_{app_id} will finish ({desyn_time +  w_iter * n_iter}) before T_end ({self.timeframe_end})"

                
                # alpha_io determines the  percentage of the task iteration time that will be spent performing I/O
                if not self.fixed_alphaio:                    
                    alpha_io = ts[app_id] / sum(ts) * self.io_limit                                    

                # update app_id
                app_id += 1

                # IO time of the task iteration
                t_io = alpha_io * w_iter
                # Computation time of the task iteration
                t_comp = w_iter - t_io

                io_load += alpha_io

                self.apps.append(self.App(n_iter=n_iter, w_iter=w_iter, alpha_io=alpha_io, t_io=t_io, t_comp=t_comp,
                                          app_id=app_id, mu=mu, desyn_time=desyn_time))

            index += 1

        

        assert io_load  <= self.io_limit + 0.1, f"Error: IO-Load: {io_load} bigger than IO-Limit:{self.io_limit}"

    def apps_to_csv(self, csv_file):
        """
        Write a csv file with all the generated apps
        :param csv_file: full path to the output csv file
        """

        print(f"\t\tWriting {csv_file}...")

        head = ['app_id', 'Tcomp', 'Tio', 'iter', 'witer',
                'alpha_io', 'compute_size', 'io_size', 'mu', 'desyn_time']

        with open(csv_file, "w") as f:
            writer = csv.writer(f)
            writer.writerow(head)

            for app in self.apps:
                writer.writerow(
                    [app.app_id, app.t_comp, app.t_io, app.n_iter, app.w_iter, app.alpha_io, app.compute_size,
                     app.io_size, app.mu, app.desyn_time])

    def print_apps(self):
        """
        print all the generated apps
        """
        print("\t# Applications:")
        for app in self.apps:
            print(app)

    def sort_apps_by_groupid(self):
        self.apps.sort(key=lambda app: app.set_id, reverse=True)

    def get_apps_in_set(self, set_id: int):
        group_list = []

        for app in self.apps:
            if app.set_id == set_id:
                group_list.append(app)

        return group_list

    def compute_group_mean_witer(self, group_id: int):
        apps = self.get_apps_in_set(group_id)

        return statistics.mean([app.w_iter for app in apps])
