//
// Created by luan on 27/10/2021.
//

#include "io_scheduler.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(io_scheduler,
                             "Messages specific for the io_scheduler");


/* Public */
//// All public methods need to be wrapped by a semaphore

void IOScheduler::add_activity(const simgrid::s4u::IoPtr &new_activity, double priority) {
    //the idea of this scheduler is that applications with the same priority cannot
    //run at the same time, but applications with different priorities can.
    semaphore->acquire();

    //first, we check if the priority already exist in the priority locker,
    //if not, we add it
    if(priority_locker.find(priority) == priority_locker.end())
        priority_locker.insert(std::pair<double, bool>(priority,false));

    //Check if there is another activity with the same priority,
    //if not we include it at the running list
    if (!priority_locker.find(priority)->second) {
        suspend_all();
        running.emplace_back(new_activity, priority);
        priority_locker.find(priority)->second = true;

        resume_all();

//        XBT_INFO("Running: %zu", running.size());
//        XBT_INFO("Waiting: %zu", waiting.size());
//        xbt_assert( running.size() <= 50, "ERROR!");


        //update time tracker
        start_time.insert(std::pair<simgrid::s4u::IoPtr, double>(new_activity, simgrid_get_clock()));


    } else // if yes we have to include the new_activity at the waiting list
        waiting.emplace_back(new_activity, priority);

    semaphore->release();
}

void IOScheduler::remove_activity(const simgrid::s4u::IoPtr &old, double priority) {
    semaphore->acquire();

    // remove activity from the running list
    remove_it(running, old);


    // Update end time
    end_time.insert(std::pair<simgrid::s4u::IoPtr, double>(old, simgrid_get_clock()));

    simgrid::s4u::IoPtr *next = nullptr;
    // check if there is a waiting activity with the same priority on the waiting list
    for (auto &itw: waiting)
        if (itw.second == priority) {
            next = &itw.first;
            break; // Getting the first at the vector
        }

    if (next != nullptr) {

        suspend_all();
        running.emplace_back(*next, priority);


        resume_all();
        start_time.insert(std::pair<simgrid::s4u::IoPtr, double>(*next, simgrid_get_clock()));

        // remove it from the waiting vector
        remove_it(waiting, (*next));
    }else
        // update priority locker so the next activity can run
        priority_locker.find(priority)->second = false;


    semaphore->release();
}

void IOScheduler::print_all() {
    semaphore->acquire();

    XBT_INFO("# Running Activities: %zu", running.size());
    XBT_INFO("# Waiting Activities: %zu", waiting.size());

    for (const auto &it: running)
        XBT_INFO("%s Remaining: %.2fBytes Priority: %f ", it.first->get_cname(),
                 it.first->get_remaining(),
                 it.second);

    semaphore->release();
}

double IOScheduler::get_io_start(const simgrid::s4u::IoPtr &activity) {
    double time = -1;

    semaphore->acquire();


    auto start = start_time.find(activity);

    if (start != start_time.end())
        time = start->second;

    semaphore->release();

    return time;
}

double IOScheduler::get_io_end(const simgrid::s4u::IoPtr &activity) {
    double time = -1;

    semaphore->acquire();


    auto end = end_time.find(activity);

    if (end != end_time.end())
        time = end->second;

    semaphore->release();

    return time;
}

double IOScheduler::get_runtime(const simgrid::s4u::IoPtr &activity) {

    double runtime = -1;

    semaphore->acquire();

    auto start = start_time.find(activity);
    auto end = end_time.find(activity);
    if (start != start_time.end() && end != end_time.end())
        runtime = end->second - start->second;

    semaphore->release();

    return runtime;
}


/* Private */
//TODO: improve this
void IOScheduler::remove_it(std::vector<std::pair<simgrid::s4u::IoPtr, double>> &activities,
                            const simgrid::s4u::IoPtr &to_remove) {
    int i = 0;
    int pos = -1;

    while (i < activities.size())
        if (activities[i].first == to_remove) {
            pos = i;
            break;
        } else
            i++;

    if (pos != -1)
        activities.erase(activities.begin() + pos);
}


//return true if there is a running application with the given priority
//bool IOScheduler::has_priority(double priority) {
//    for (const auto &it: running)
//        if (it.second == priority)
//            return true;
// return false;
//}

void IOScheduler::suspend_all() {
    for (const auto &act: running)
        act.first->suspend();
}

void IOScheduler::resume_all() {
    for (const auto &act: running)
        act.first->resume();
}






