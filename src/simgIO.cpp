/* Copyright (c) 2007-2021. The SimGrid Team. All rights reserved.          */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */


// --log=io_sim.thresh:verbose

#include "simgIO.h"
#include "io_scheduler.h"
#include <boost/program_options.hpp>


XBT_LOG_NEW_DEFAULT_CATEGORY(simgio,
                             "Messages specific for the io_sim");

IOScheduler &getScheduler() {
    // Initialized upon first call to the function.
    static IOScheduler scheduler;
    return scheduler;
}

simgrid::s4u::SemaphorePtr &getSemaphore() {
    static simgrid::s4u::SemaphorePtr semaphore = simgrid::s4u::Semaphore::create(1);
    return semaphore;
}

// global variables
// number of apps executed during the simulation
size_t n_apps;

// parameters used to compute the stretch and the occupation
double total_effective = 0.0;
double total_effective_cpu = 0.0;
double total_effective_io = 0.0;


double timeFrame_start, timeFrame_end;

// Stores the start time, end time and stretch of each app
std::map<std::string, std::vector<double>> apps_info;


// Stores the start time, end time of all phases of each app
std::map<std::string, std::vector<std::vector<double>>> apps_phases;

// global stretch
double global_stretch = 0.0;

// Parameters for tests with noise
std::default_random_engine rd;
float noise = 0.0;
bool with_noise = false;



double timeFrame_cpu(double start, double end, double *total) {
    double effective_time = 0.0;

    // If the execution start inside the timeFrame we have two cases:
    if (start >= timeFrame_start && start < timeFrame_end) {
        // Case 1: completely inside the timeframe
        if (end <= timeFrame_end) {
            effective_time = end - start;
            *total += effective_time; // consider all time
            // Case 2: Finished outside the timeframe
        } else if (end > timeFrame_end) {
            effective_time = timeFrame_end - start;
            *total += effective_time;
        } // consider only the time inside the timeframe
    } else {
        // if the execution start before the timeframe, we have two cases:
        if (start < timeFrame_start) {
            // case 1: end of the execution are inside the timeFrame
            if (end > timeFrame_start && end <= timeFrame_end) {
                effective_time = end - timeFrame_start;
                *total += effective_time; // consider only the time inside the timeframe
                // case 2: end of the execution are outside the timeframe
            } else if (end > timeFrame_end) {
                effective_time = timeFrame_end - timeFrame_start;
                // remove the execution time before the beginning and after the end of the timeframe
                *total += effective_time;
            }
        }
    }
    return effective_time;
}

double timeFrame_io(double start, double end, double *total_bytes, sg_size_t io_size, std::string name) {


    double effective_time = 0.0;
    //Compute the Actual bandwidth of the current operation
    auto B = (double) io_size / (end - start);

    // If the IO started inside the timeFrame we have two cases:
    if (start >= timeFrame_start && start < timeFrame_end) {
        if (end <= timeFrame_end) {
            // Case 1: completely inside the timeframe
            effective_time = end - start;
            *total_bytes += (double) io_size; // Get all bytes
        } else if (end > timeFrame_end) {
            // Case 2: IO Finished outside the timeframe
            effective_time = (timeFrame_end - start);
            *total_bytes += B * effective_time;
        }
    } else {
        // if the IO started before the timeframe, we have two cases:
        if (start < timeFrame_start) {
            if (end > timeFrame_start && end <= timeFrame_end) {
                // Case 1: IO finished inside the timeFrame
                effective_time = end - timeFrame_start;
                // Get only the bytes Written during the timeFrame
                *total_bytes += B * effective_time;
            } else if (end > timeFrame_end) {
                // Case 2: IO Finished after the end of the timeFrame
                effective_time = timeFrame_end - timeFrame_start;
                *total_bytes += B * effective_time;
            }
        }
    }

    return effective_time;
}

static void host(std::vector<std::string> args) {
    // Get all input args
    auto name = args[1];  // Getting app name
    int iterations = std::stoi(args[2]);
    double c_size = std::stod(args[3]);
    sg_size_t io_size = std::stoll(args[4]);
    double priority = std::stof(args[5]);
    double group_id = std::stof(args[6]);
    double desyn_time = std::stof(args[7]);


    static std::uniform_real_distribution<float> gamma{0, 1};



    // Get disk
    const simgrid::s4u::Disk *disk = simgrid::s4u::Host::current()->get_disks().front();

    // Computing witer
    n_apps = simgrid::s4u::Host::current()->get_actor_count();
    auto Tcomp = c_size / simgrid::s4u::Host::current()->get_speed();
    auto Tio = (double) io_size / disk->get_read_bandwidth();
    auto witer = Tcomp + Tio;

    XBT_VERB("%s-(%1.f): %gFlops %llu Bytes; Witer: %.2f Tcomp:%.2f Tio:%.2f", name.c_str(), priority, c_size, io_size,
             witer, Tcomp, Tio);


    // check if the number of apps is small than the number of cores
    xbt_assert(n_apps < simgrid::s4u::Host::current()->get_core_count(),
               "Error: Number of Apps (%zu) should be less or equal than the number of cores (%d)", n_apps,
               simgrid::s4u::Host::current()->get_core_count());


    // start the distribution with the noise percentage. Used only if when with_noise=True parameters
    static std::uniform_real_distribution<float> d{-1 * noise, noise};



    /*Time trackers Variables*/
    double start_exec, end_exec;
    double total_cpu = 0.0;
    double total_bytes = 0;

    std::vector<std::vector<double>> times;

    // this variable accumulates all the effective CPU and IO time (performed inside the time frame)
    // it is used to compute the occupation
    double acc_effective_cpu = 0.0;
    double acc_effective_io = 0.0;

    // this variable accumulates the effective iterations
    double acc_effective_iter = 0.0;

    // Execution loop
    auto start_app = simgrid_get_clock();

    for (int i = 0; i < iterations; i++) {

        // If it is the first iteration, we need to insert the desyn time (if there is any)
        // desynchonize application
        if (i == 0 && desyn_time > 0) {
            XBT_VERB("Task: %s sleep for %f\n", name.c_str(), desyn_time);
            simgrid::s4u::this_actor::sleep_for(desyn_time);
            // if the desyn time was include, we need to update the start_app time
            start_app = simgrid_get_clock();
        }

        //  First, let's create the execution phase in parallel to avoid CPU sharing

        auto csize_tmp = c_size;
        auto iosize_tmp = io_size;

        // Check if we need to add noise
        // In this case, we add noise to csize_tmp and iosize_tmp
        // keeping the original "sizes" so the noise is not accumulated through the execution
        if (with_noise) {
            csize_tmp = c_size * (1 + d(rd));
            iosize_tmp = io_size * (1 + (sg_size_t) d(rd));

        }


        std::vector<double> comp(1, csize_tmp);
        std::vector<double> comm(0.0, 0.0); // set communication to zero (mandatory to the simulation)
        std::vector<simgrid::s4u::Host *> multicore_overload(1, simgrid::s4u::Host::current());

        /* Execution phase */
        // Tracking time (start)
        start_exec = simgrid_get_clock();
        simgrid::s4u::this_actor::parallel_execute(multicore_overload, comp, comm);
        // Tracking time (end)
        end_exec = simgrid_get_clock();

        // the effective_cpu keeps only processing time that was performed inside the timeFrame
        // It is used to compute the stretch and the occupation metrics
        auto effective_cpu = timeFrame_cpu(start_exec, end_exec, &total_cpu);


        /* IO phase */
        // Create the IO activity (associating it to the disk)
        simgrid::s4u::IoPtr io_activity = disk->io_init(iosize_tmp, simgrid::s4u::Io::OpType::WRITE);
        io_activity->set_priority(priority); // Set priority

        io_activity->start();  // At this point, it is important to start and suspend the IO activity.
        io_activity->suspend(); // Otherwise, we cannot restart it later

        // check if io_activity started before expected
        xbt_assert(io_activity->get_remaining() == (double) iosize_tmp, "IO activity start before expected");

        // Send the IO activity to the IO scheduler
        getScheduler().add_activity(io_activity, priority);
        while (not io_activity->test()) // waiting for the end of the IO
            simgrid::s4u::this_actor::sleep_for(0.1);

        //  Once the IO operation is completed, we remove it from the scheduler.
        // TODO: The scheduler should do it by itself, but for sake of simplicity, let's keep it here for now.
        getScheduler().remove_activity(io_activity, priority);



        // the effective_io keeps only the IO time that was performed inside the timeFrame
        // It is used to compute the stretch and the occupation metrics
        auto effective_io = timeFrame_io(getScheduler().get_io_start(io_activity),
                                         getScheduler().get_io_end(io_activity),
                                         &total_bytes, iosize_tmp, name);


        // compute iterations
        auto total_time = (end_exec - start_exec) +
                          (getScheduler().get_io_end(io_activity) - getScheduler().get_io_start(io_activity));

        acc_effective_iter += (effective_cpu + effective_io) / total_time;



        // We accumulate all the effective work (cpu + io times)
        // and the effective cpu
        // Further, we use it to compute the occupation
        acc_effective_cpu += effective_cpu;
        acc_effective_io += effective_io;


        XBT_VERB("\n%s-(%1.f)-%d/%d:\n"
                 "\tEffective-CPU: %.2f\n"
                 "\tEffective-IO: %.2f\n", name.c_str(), priority, i + 1, iterations,
                 effective_cpu,
                 effective_io);


        /* let's keep it out of the code for now, but it can be used in the future */
        // When the execution phase and the IO phase is finished, we store the start and end times
        // Further, it will put on a CSV file (for visualization purposes)
        std::vector<double> phase_values = {
                start_exec,
                end_exec,
                getScheduler().get_io_start(io_activity),
                getScheduler().get_io_end(io_activity)
        };
        times.push_back(phase_values);

    }

    //  At this point, the application has finished
    // get the simulated end time
    auto end_app = simgrid_get_clock();

    // Computing the stretch
    double stretch = 0.0;
    if (total_cpu > 0 || total_bytes > 0)
        stretch = (timeFrame_end - timeFrame_start) / (total_cpu + (total_bytes / disk->get_read_bandwidth()));


    XBT_VERB("\n ######################################"
             "\n%s-(%1.f):"
             "\n\tACC-CPU: %.2f"
             "\n\tACC-IO: %.2f"
             "\n\tACC-Iter: %.2f"
             "\n\tOptimal Time (W/T noise): %.2f"
             "\n\tTotal Time: %.2f; "
             "\n\tStretch: %.2f",
             name.c_str(), priority,
             acc_effective_cpu,
             acc_effective_io,
             acc_effective_iter,
             iterations * witer,
             end_app - start_app,
             stretch);


    /* Critical Area */
    getSemaphore()->acquire();

    //update global stretch
    if (stretch > global_stretch)
        global_stretch = stretch;

    // Accumulate the total effective time of all applications
    // Use further to compute the occupation
    total_effective += acc_effective_cpu + acc_effective_io;
    total_effective_cpu += acc_effective_cpu;
    total_effective_io += acc_effective_io;

    // store app stretch values
    std::vector<double> info_values = {start_app,
                                       end_app,
                                       acc_effective_cpu,
                                       acc_effective_io,
                                       acc_effective_iter,
                                       total_bytes,
                                       stretch,
                                       Tcomp,
                                       Tio,
                                       (double) iterations,
                                       priority,
                                       group_id};

    apps_info.insert(std::pair<std::string, std::vector<double>>(name, info_values));

    // Let's keep it out of the code from now. It can be used in the future
    // store apps_times (used to generate the CSV)
    apps_phases.insert(std::pair<std::string, std::vector<std::vector<double>>>(name, times));


    getSemaphore()->release();
}


class InputParser {
    // courtesy of  @author iain
    // https://stackoverflow.com/questions/865668/parsing-command-line-arguments-in-c
public:
    InputParser(int &argc, char **argv) {
        for (int i = 1; i < argc; ++i)
            this->tokens.emplace_back(argv[i]);
    }

    const std::string &getCmdOption(const std::string &option) const {
        std::vector<std::string>::const_iterator itr;
        itr = std::find(this->tokens.begin(), this->tokens.end(), option);
        if (itr != this->tokens.end() && ++itr != this->tokens.end()) {
            return *itr;
        }
        static const std::string empty_string;
        return empty_string;
    }

    bool cmdOptionExists(const std::string &option) const {
        return std::find(this->tokens.begin(), this->tokens.end(), option)
               != this->tokens.end();
    }

private:
    std::vector<std::string> tokens;
};


void help_msg(bool error) {
    if (error)
        std::cout << "Error: not all requirement arguments were passed as input.\n";

    std::cout
            << "Synopsis: ./simgio -p <platform_file>.xml -d <deployment_file>.xml "
               "-ts <timeFrame_start> -te <timeFrame_end>\n" << std::endl;

    std::cout << "Required arguments\n" << std::endl;
    std::cout << " -p\t" << "Full path to the platform file" << std::endl;
    std::cout << " -d\t" << "Full path to the deployment file" << std::endl;
    std::cout << " -ts\t" << "Time frame start (Tbegin) " << std::endl;
    std::cout << " -te\t" << "Time frame end (Tend) " << std::endl;

    std::cout << "\n\n" << std::endl;
    std::cout << "Optional arguments\n" << std::endl;

    std::cout << " --info" << "\t\tFull path to a CSV file with the execution info of each application. \n"
                              "\t\tThis file includes simulated start and end times, and the stretch per application.\n"
                              "\t\tThis file is created only if this argument is passed as input.\n" << std::endl;
    std::cout << " --phases"
              << "\tFull path to a CSV file with information about execution phases of each application\n"
                 "\t\tThis file includes the start and ends times of all applications' compute and I/O phases.\n"
                 "\t\tThe file is created only if this argument is passed as input.\n" << std::endl;

    std::cout << " --noise" << "\tRange bound (b) to the noise used in the aperiodic application protocol.\n"
                               "\t\tNoise is introduced only if this parameter is passed as input.\n" << std::endl;

    std::cout << " --seed" << "\t\tSeed used on the uniform distribution to draw the noise variables.\n" << std::endl;

    std::cout << "\n" << std::endl;
    exit(-1);


}

int main(int argc, char *argv[]) {
    simgrid::s4u::Engine e(&argc, argv);


    // Arguments will be stored here
    InputParser input(argc, argv);

    if (input.cmdOptionExists("-h"))
        help_msg(false);


    const std::string &platform = input.getCmdOption("-p");
    const std::string &deployment = input.getCmdOption("-d");
    const std::string &app_info_file = input.getCmdOption("--info");
    const std::string &app_phases_file = input.getCmdOption("--phases");

    auto str_frameStart = input.getCmdOption("-ts");
    auto str_frameEnd = input.getCmdOption("-te");

    // variable used to store the seed used on execution with noise
    int seed = 0;


    if (platform.empty() || deployment.empty() || str_frameStart.empty() || str_frameEnd.empty()) {
        help_msg(true);
        exit(1);
    }

    sg_storage_file_system_init();
    e.load_platform(platform);
    e.register_function("host", host);
    e.load_deployment(deployment);

    // convert time frame and check the values
    timeFrame_start = std::stof(str_frameStart);
    timeFrame_end = std::stof(str_frameEnd);
    xbt_assert(timeFrame_end > timeFrame_start, "\nStart of the time frame should be smaller than its end");

    // check if the noise parameter was passed
    const std::string &str_noise = input.getCmdOption("--noise");
    const std::string &str_seed = input.getCmdOption("--seed");

    if (!str_noise.empty()) {
        noise = std::stof(str_noise);
        with_noise = true;

        // set up seed
        seed = time(nullptr);
        // if seed was given as an argument, use the argument (reproducibility)
        if (!str_seed.empty())
            seed = std::stoi(str_seed);

        rd.seed(seed);
    }

    e.run();

    xbt_assert(apps_info.size() == n_apps,
               "ERROR: Inconsistency with the number of APPS. Do all the apps have a unique id?");

    // compute the occupation (utilization)
    // double occupation = total_effective / ((double) n_apps * (timeFrame_end - timeFrame_start));

    double utilization = total_effective_cpu / ((double) n_apps * (timeFrame_end - timeFrame_start));

    //compute mean stretch
    double mean_stretch;
    double sum_stretch = 0.0;
    for (const auto &it: apps_info)
        sum_stretch += it.second[6];
    mean_stretch = sum_stretch / (double) n_apps;


    std::cout << "max_stretch " << global_stretch << "\n";
    std::cout << "mean_stretch " << mean_stretch << "\n";
    std::cout << "utilization " << utilization << "\n";
    std::cout << "noise " << noise << "\n";
    std::cout << "noise seed " << seed << "\n";

    XBT_VERB("Applications:");
    for (auto &it1: apps_info)
        XBT_VERB("%s Stretch: %.2f", it1.first.c_str(), it1.second[6]);

    // saving APPs info to a CSV FILE
    if (!app_info_file.empty()) {
        //XBT_INFO("Saving APP INFO in %s", app_info_file.c_str());
        // Generate the csv file
        std::ofstream myfile;
        myfile.open(app_info_file);
        myfile << "app_id,"
                   "start_app,"
                   "end_app,"
                   "effective_cpu,"
                   "effective_io,"
                   "effective_iter,"
                   "total_bytes,"
                   "stretch,"
                   "utilization,"
                   "Tcomp,"
                   "Tio,"
                   "iterations,"
                   "priority,"
                   "group_id\n";
        for (const auto &it: apps_info) {
            myfile << it.first.c_str() << ","
                   << it.second[0] << "," //start_app
                   << it.second[1] << "," //end_app
                   << it.second[2] << "," //acc_effective_cpu
                   << it.second[3] << "," //acc_effective_io
                   << it.second[4] << "," //acc_effective_iter
                   << it.second[5] << "," //total bytes  inside the time stamp
                   << it.second[6] << "," //stretch
                   << utilization << "," //utilization
                   << it.second[7] << "," //Tcomp
                   << it.second[8] << "," //Tio
                   << it.second[9] << "," //iterations
                   << it.second[10] << "," //priority
                   << it.second[11] << "\n"; //group_id
        }
        myfile.close();
    }

    // saving app phases to CSV file
    if (!app_phases_file.empty()) {
        std::ofstream myfile;
        myfile.open(app_phases_file);
        myfile << "app_id,start_exec,end_exec,start_io,end_io\n";
        for (const auto &it: apps_phases) {
            auto app_id = it.first.c_str();
            for (const auto &vet: it.second) {
                myfile << app_id << ","
                       << vet[0] << "," //start_exec
                       << vet[1] << "," //end_exec
                       << vet[2] << "," //start_io
                       << vet[3] << "\n"; //end_io
            }
        }
        myfile.close();
    }


    return 0;
}
