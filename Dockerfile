FROM ubuntu:20.04

ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ADD . /root/iosets/
WORKDIR /root/iosets/

# getting the dependencies
RUN apt update -y && apt install python3 python3-pip  g++ cmake libboost-dev libboost-context-dev vim git -y

# https://framagit.org/simgrid/simgrid
# git checkout 2618e575d819b0d0046c6c35a4ae96e94b61d0be;

# instaling simgrid
RUN git clone https://framagit.org/simgrid/simgrid  && \
    cd simgrid; git checkout 2618e575d819b0d0046c6c35a4ae96e94b61d0be;  cmake -DCMAKE_INSTALL_PREFIX=/opt/simgrid .; make; make install

# INSTALL PYsimgio
RUN pip install python-dateutil --upgrade && pip install setuptools && pip install -e . && pip install -r PYsimgio/requirement.txt

# compile simgio
RUN mkdir -p build; cd build; rm -rf *; cmake ../; make
