#include <stdio.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <stdbool.h>
#include <unistd.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <assert.h>
#include <math.h>

#define PORT 2022
#define CLIENT_MAX 100
#define SET_MAX 100
#define ID_LEN 100
#define MSG_MAX 255

#define EXCLUSIVE true
#define SETTEN false
#define FAIRSHARE false


int s;
int connec[CLIENT_MAX];
int connec_index=0;
char id[CLIENT_MAX][ID_LEN];
pthread_mutex_t mutex=PTHREAD_MUTEX_INITIALIZER;
pthread_t read_thr[CLIENT_MAX];
bool debug = false;

/* information about the apps and queues*/
bool writing[CLIENT_MAX]; //is this application writing right now?
double witer[CLIENT_MAX]; //the witer for each application
int set[CLIENT_MAX]; //the set each application belongs to
int waiting[SET_MAX][CLIENT_MAX]; //for each set, a FIFO of applications that are waiting
bool set_writing[SET_MAX];

void end_if_error(bool error_condition, char *msg) {
    if (error_condition) {
        perror(msg);
        exit(EXIT_FAILURE);
    }
}

void authorize(int app) {
    int ret;
    int msg = 1;
    if(debug) printf("Authorizing app %d (witer %lf, set %d) to do I/O\n", app, witer[app], set[app]);
    writing[app] = true;
    set_writing[set[app]] = true;
    ret = write(connec[app], &msg, sizeof(int));
    end_if_error(ret != sizeof(int), "send authorization");
}

//TODO this queue is VERY inefficient, it's just lazy coding. It should be fine because in our tests we do not have a huge number of apps (but if we increase the tests then we need to redo this)
void add_to_queue(int app) {
    if(debug) printf("Adding app %d to the queue of set %d\n", app, set[app]);
    int i;
    for(i = 0;  i < CLIENT_MAX; i++) {
        if (waiting[set[app]][i] == -1) {
            waiting[set[app]][i] = app;
            break;
        }
    }
    assert(i < CLIENT_MAX);
}
int remove_from_queue(int set) {
    assert(waiting[set][0] != -1);
    int ret = waiting[set][0];
    int i;
    for(i = 0; (i< CLIENT_MAX-1) && (waiting[set][i] >= 0); i++) {
        waiting[set][i] = waiting[set][i+1];
    }
    if (i == CLIENT_MAX-1)
        waiting[set][i] = -1;
    return ret;
}

void * chat (void *arg) {
    int me = (int)(intptr_t)arg;
    int ret;
    int msg;
    writing[me] = false;
    while(true) {
        if(debug) printf("waiting for message from application %d\n", me);
        ret = read(connec[me], &msg, sizeof(int));
        if(ret <= 0) {
            printf("broken connection with app %d\n", me);
            return 0;
        }
        //msg can be
        //1 I WANT TO DO IO
        //2 I'M DONE WITH IO PHASE
        //3 I'M DONE WITH EXECUTION
        switch(msg) {
            case 1:
                if(debug) printf("application %d (witer %lf) wants to do IO\n", me, witer[me]);
                assert(!writing[me]);
                pthread_mutex_lock(&mutex);
                if (!set_writing[set[me]]) { //no one from my set is doing i/o
                    authorize(me);
                } else { //add this request to the queue
                    add_to_queue(me);
                }
                pthread_mutex_unlock(&mutex);
                break;
            case 2:
                if(debug) printf("application %d (witer %lf) is done with the IO phase\n", me, witer[me]);
                assert(writing[me]);
                pthread_mutex_lock(&mutex);
                writing[me] = false;
                set_writing[set[me]] = false;
                if (waiting[set[me]][0] != -1) { //there is another app from the same set waiting to do io
                    int other = remove_from_queue(set[me]);
                    assert(other >= 0 && other != me);
                    authorize(other);
                }
                pthread_mutex_unlock(&mutex);
                break;
            case 3:
                if(debug) printf("application %d (witer %lf) is done with its execution\n", me, witer[me]);
                //end the connection
                assert(!writing[me]);
                close(connec[me]);
                return 0; //this thread dies
            default:
                printf("Panic! I don't know what the application %d is trying to tell me with msg=%d?\n", me, msg);
                return 0;
        }
    }
    return 0;
}

int get_set(double witer, int app) {
    if(EXCLUSIVE)
        return 0;
    else if(FAIRSHARE)
        return app;
    else if(SETTEN)
        return round(log10(witer));
    else {
        //TODO set2???
        assert(false);
    }
}

// run by a thread that receives connection requests and accepts them, then create a new thread to receive requests from that connection
void * accept_connections(void *arg) {
    struct sockaddr_in client_addr;
    unsigned int len;
    int ret;

    while(true) {
        //wait for a connection and accept it
        len = sizeof(client_addr);
        memset(&client_addr, 0, len);
        if(debug) printf("Server is waiting for connections\n");
        connec[connec_index] = accept(s, (struct sockaddr *)&client_addr, &len);
        end_if_error(connec[connec_index] < 0, "accept");
        //get the witer of the new application
        ret = read(connec[connec_index], witer + connec_index, sizeof(double));
        end_if_error(ret <= 0,"read witer");
        end_if_error(witer[connec_index] <= 0,"bad witer");
        set[connec_index] = get_set(witer[connec_index], connec_index);
        printf("Accepted connection from app with witer = %lf, app %d\n", witer[connec_index], connec_index);
        connec_index++;
        //create a thread to read from this connection
        ret = pthread_create(read_thr+connec_index-1, NULL, chat, (void *)(intptr_t)connec_index-1);
        end_if_error(ret != 0, "pthread_create 0");
        if (connec_index >= CLIENT_MAX) { //TODO make it wait until it can and then accept more connections, if needed for our tests
            printf("The server is no longer accepting connections\n");
            break; //we can have only this number of active connections.
        }
    }
    return 0;
}

int main (int argc, char *argv[]) {
    if(EXCLUSIVE)
        fprintf(stderr, "Warning - using exclusive scheduler!\n");
    else if(FAIRSHARE)
        fprintf(stderr, "Warning - using fairshare scheduler!\n");
    else if (SETTEN)
        fprintf(stderr, "Warning - using set10 scheduler!\n");
    else {
        fprintf(stderr, "Panic! I don't have a scheduler!\n");
        return EXIT_FAILURE;
    }
    int ret;
    struct sockaddr_in server_addr;
    pthread_t conn_thr;
    //create a tcp socket
    s = socket(AF_INET, SOCK_STREAM, 0);
    end_if_error(s == -1, "socket");
    //bind it to my server address
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);
    server_addr.sin_addr.s_addr = INADDR_ANY;
    ret = bind(s, (struct sockaddr *)&server_addr, sizeof(server_addr));
    end_if_error(ret == -1, "bind");
    //set this socket as a passive socket that will be used to accept connections
    ret = listen(s, 100);
    end_if_error(ret == -1, "listen");
    //initialize the queues
    for(int i = 0; i < SET_MAX; i++) {
        for(int j=0; j < CLIENT_MAX; j++)
            waiting[i][j] = -1;
        set_writing[i] = false;
    }
    //create a thread that will wait for connections
    ret = pthread_create(&conn_thr, NULL, accept_connections, NULL);
    end_if_error(ret != 0, "pthread_create 1");
    //close and end
    pthread_join(conn_thr, NULL);
    for(int i = 0; i < connec_index; i++) {
        pthread_join(read_thr[i], NULL);
    }
    close(s);
    return EXIT_SUCCESS;
}

