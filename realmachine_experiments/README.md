# Practical Experiments (Section VI-A)

In this folder you'll find all code used to generate the practical results and the results themselves.

- The `ior_schedclient` folder contains IOR 3.4.0 modified to talk to the scheduler using sockets (with TCP). It will receive as an argument the IP of the server. 
This implementation only works for periodic write phases (with no reading).
It has been compiled and executed with OpenMPI 4.0.3.
The new command-line options are -p for the IP of the scheduler and -P for the witer. An estimate of witer must be provided, because that information is used by the scheduler (it does not compute it automatically).

- The `scheduler` folder contains the scheduler. In the beginning of the .c we can change the scheduling algorithm and the verbosity level (debug as true or false).
The scheduler must be running before starting the IOR instances.

- In the `run_scripts` folder, the `run_with_scheduler` and `run_without_scheduler` scripts were used in slurm scripts to run the experiments. They call `run_concior_test.py` to start the concurrent IOR instances. 
All parameters used to call IOR are configured in the beginning of the python script.

- In the `parse_results` folder, we can find the python scripts use to calculate the reported metrics from the results (stretch, utilization, etc).

- The `results` folder contains all results, one test per folder. Inside each test folder, we can find the standard output of each IOR instance and of the scheduler. **This was omitted for double blind review because the output mentions the folder where tests were executed, including the username*.*


