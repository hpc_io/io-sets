import sys
import subprocess
from time import sleep

#some hard-coded parameters
high_procs = 160
test_duration = 6400 #in seconds
nodes = 8
hf_procs = 160
lf_procs = 16
ior = "./ior_schedclient/src/ior"
ior_general_parameters = "-F -t 1M -w"
output_path = "/beegfs/[USER]/8targets/iosets_"
hf_block = "204M"
lf_block = "22400M"
hf_iter = 100
lf_iter = 10
hf_comp = 60500000 #in microseconds
lf_comp = 603500000
hf_witer = 64000000 #in microseconds
lf_witer = 640000000
scheduler_path = "./ior_schedclient/scheduler/sched_"


schedulers=["set10", "exclusive", "fairsharewithsched"]
use_server = ["set10", "exclusive", "fairsharewithsched"]
priority_based = ["set10"]
for i in use_server:
    assert i in schedulers

#get arguments
if len(sys.argv) != 6:
    print(f"Usage: python3 {sys.argv[0]} <number of high-frequency> <number of low-frequency> <scheduler> <ip of the scheduler> <output path>")
    print("scheduler is one of "+str(schedulers))
    exit(1)
nh = int(sys.argv[1])
nl = int(sys.argv[2])
napps = nh+nl
print(f"I will generate {nh} high-frequency apps and {nl} low-frequency ones, for a total of {napps} applications")
scheduler = sys.argv[3]
assert scheduler in schedulers
node_sched = sys.argv[4]
log_path = sys.argv[5]
subprocess.run("mkdir "+log_path, shell=True)
if log_path[-1] != "/":
    log_path+= "/"

#generate application command lines
commands = []
for i in range(napps):
    if i < nh:
        #make a high-frequency app
        if scheduler in priority_based:
            #the larger number of processes gives a higher priority for the high-frequency app
            proc = hf_procs
            b = hf_block
        else:
            #we will use the same number of procs for all the apps, but then since we are decreasing
            #the number of processes of these applications, we need to increase the amount of data
            #in order to make it keep the same duration of the I/O phase (which is important because
            #we want each phase to reach peak I/O performance by itself, and also because of witer)
            proc = lf_procs
            assert "M" in hf_block
            assert hf_procs > lf_procs
            b = str(int(hf_block.split('M')[0]) * (hf_procs//lf_procs))+"M"
        itera = hf_iter
        d = hf_comp
        witer = hf_witer
    else:
        #make a low-frequency app
        proc = lf_procs
        b = lf_block
        itera = lf_iter
        d = lf_comp
        witer = lf_witer
    com = "timeout "+str(test_duration)+" srun --nodes="+str(nodes)+" --ntasks="+str(proc)+" "
    com+= "--ntasks-per-node="+str(proc//nodes)+" "
    com+= "--oversubscribe --overlap "+ior+" "+ior_general_parameters+" "
    com+= "-b "+b+" -o "+output_path+str(i)+"_ -i "+str(itera)+" -d "+str(d)+" "
    if scheduler in use_server:
        com+= "-p "+node_sched+" -P "+str(witer) +" "
    com+= "> "+log_path+"ioroutput_app"+str(i)+".txt & "
    commands.append(com)
assert len(commands) == napps

print("starting applications...")
for c in commands:
    print(c)
    subprocess.run(c, shell=True)
print("sleeping for "+str(test_duration+5)+"...")
sleep(test_duration+5)
print("will stop all processes")
subprocess.run("killall -9 srun", shell=True)
sleep(10)
print("done with the test.")
