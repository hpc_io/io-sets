hf=$1
lf=$2
scheduler=$3
rep=$4
node_sched=$5
ip_sched=$6

echo "I will run $hf high-freq and $lf low-freq with $scheduler scheduler, this is the repetition number ${rep}. The scheduler will run in node ${node_sched} (${ip_sched})."

folder="iosets_experiments/${hf}hf_${lf}lf_${scheduler}_${rep}"
largetimelimit=6600

  ## Aggregate Execution
date
rm -rf $folder
mkdir $folder
echo "starting the scheduler at" ${node_sched} ${ip_sched}
echo timeout $largetimelimit srun --ntasks=1 --exclusive --nodelist=${node_sched} ior_schedclient/scheduler/sched_${scheduler} > ${folder}/sched_output.txt 
timeout $largetimelimit srun --ntasks=1 --exclusive --nodelist=${node_sched} ior_schedclient/scheduler/sched_${scheduler} > ${folder}/sched_output.txt &
sleep 10
echo "running the test"
python3 run_concior_test.py ${hf} ${lf} $scheduler ${ip_sched} $folder
echo "the test is done, stopping the scheduler"
killall -9 sched_${scheduler}
echo "done"
date



