import subprocess
from parse_iosets_exp_folder import parse_folder
from numpy import mean, median

def get_stats(a_list, unit):
    mini = min(a_list)
    maxi = max(a_list)
    meani = mean(a_list)
    mediani = median(a_list)
    num = len(a_list)
    stri = "min "+str(mini)+" max "+str(maxi)+" mean "+str(meani)+" median "+str(mediani)+" "+unit+" (measured from "+str(num)+" values)"
    return stri


#hardcoded parameters
t_start = 1300 #the timeframe where we will measure things
t_end = 6300
hf_iter = 100
lf_iter = 10
hf_phase_MB = 204*160  #size in MB of each I/O phase (for the whole app)
lf_phase_MB = 22400*16
perf= {}
#perf["160"] = 9325.7 #MB/s #I measured that 160&204M takes ~3.5s to run
#perf["16_lf"] =  9819.2 #MB/s #I measured that 16*22400M takes ~36,5s to run
#perf["16_hf"] = 10232 #I measured that 16*2040M takes ~3.19s to run
#I update the perf values after running the script a first time, because it reports me the time of phases running by themselves
perf["160"] = 10007.82
perf["16_lf"] = 10493.71
perf["16_hf"] = 10495.05
timewall = 6405 #after this many seconds, the apps will be killed
base_folder = "iosets_experiments/"

folders = subprocess.getoutput("ls "+base_folder+" | grep hf |grep lf").split("\n")
to_remove = [folder for folder in folders if ".csv" in folder]
for folder in to_remove:
    folders.remove(folder)
#print(folders)

exclusive_perf = {}
hf_set10_initial_perf= []
start_times=[]
cpu_time={}
arq = open(base_folder+"results.csv", "w")
#arq.write("scheduler;hf_apps;lf_apps;rep;stretch_set0;stretch_set1;mean_stretch;gmean_stretch;max_stretch;utilization;bw_set0;bw_set1\n")
arq.write("scheduler;hf_apps;rep;max_stretch;utilization\n")
for folder in folders:
    print(folder)
    print("===========================================================================")
    this_folder = folder
    if this_folder[-1] == '/':
        this_folder = this_folder[:len(this_folder)-1]
    assert this_folder[-1] != '/'
    phases_file = open(base_folder+this_folder+"_phases.csv", "w")
    phases_file.write("app_id,start_exec,end_exec,start_io,end_io\n")
    stri = parse_folder(base_folder+folder, t_start, t_end, hf_iter, lf_iter, hf_phase_MB, lf_phase_MB, perf, timewall, phases_file, exclusive_perf, hf_set10_initial_perf, start_times, cpu_time)
    arq.write(stri)
    phases_file.close()
arq.close()

for setid in exclusive_perf:
    stri = get_stats(exclusive_perf[setid], "MB/s")
    print(f"performance for set {setid} phases using exclusive scheduler: {stri}")
stri = get_stats(hf_set10_initial_perf, "MB/s")
print(f"performance for set 0 phases using set-10 scheduler before the first I/O of any set 1 apps: {stri}")
print("====================")
first = min(start_times)
start_times = [t - first for t in start_times]
stri = get_stats(start_times, "")
print(f"variability in the start time of the applications: {stri}")
all_io = exclusive_perf[0]+exclusive_perf[1]+hf_set10_initial_perf
stri = get_stats(all_io, "MB/s")
print(f"variability in I/O phase performance: {stri}")
stri = get_stats(cpu_time[0], "s")
print(f"variability in compute phase duration for set 0: {stri}")
stri = get_stats(cpu_time[1], "s")
print(f"variability in compute phase duration for set 1: {stri}")


